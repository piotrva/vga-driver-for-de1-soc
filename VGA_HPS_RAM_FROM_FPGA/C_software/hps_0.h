#ifndef _ALTERA_HPS_0_H_
#define _ALTERA_HPS_0_H_

/*
 * This file was automatically generated by the swinfo2header utility.
 * 
 * Created from SOPC Builder system 'hps_fpga' in
 * file './hps_fpga.sopcinfo'.
 */

/*
 * This file contains macros for module 'hps_0' and devices
 * connected to the following masters:
 *   h2f_axi_master
 *   h2f_lw_axi_master
 * 
 * Do not include this header file and another header file created for a
 * different module or master group at the same time.
 * Doing so may result in duplicate macro names.
 * Instead, use the system header file which has macros with unique names.
 */

/*
 * Macros for device 'address_PIO', class 'altera_avalon_pio'
 * The macros are prefixed with 'ADDRESS_PIO_'.
 * The prefix is the slave descriptor.
 */
#define ADDRESS_PIO_COMPONENT_TYPE altera_avalon_pio
#define ADDRESS_PIO_COMPONENT_NAME address_PIO
#define ADDRESS_PIO_BASE 0x0
#define ADDRESS_PIO_SPAN 32
#define ADDRESS_PIO_END 0x1f
#define ADDRESS_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define ADDRESS_PIO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define ADDRESS_PIO_CAPTURE 0
#define ADDRESS_PIO_DATA_WIDTH 32
#define ADDRESS_PIO_DO_TEST_BENCH_WIRING 0
#define ADDRESS_PIO_DRIVEN_SIM_VALUE 0
#define ADDRESS_PIO_EDGE_TYPE NONE
#define ADDRESS_PIO_FREQ 50000000
#define ADDRESS_PIO_HAS_IN 0
#define ADDRESS_PIO_HAS_OUT 1
#define ADDRESS_PIO_HAS_TRI 0
#define ADDRESS_PIO_IRQ_TYPE NONE
#define ADDRESS_PIO_RESET_VALUE 0

/*
 * Macros for device 'control_PIO', class 'altera_avalon_pio'
 * The macros are prefixed with 'CONTROL_PIO_'.
 * The prefix is the slave descriptor.
 */
#define CONTROL_PIO_COMPONENT_TYPE altera_avalon_pio
#define CONTROL_PIO_COMPONENT_NAME control_PIO
#define CONTROL_PIO_BASE 0x10
#define CONTROL_PIO_SPAN 32
#define CONTROL_PIO_END 0x2f
#define CONTROL_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define CONTROL_PIO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CONTROL_PIO_CAPTURE 0
#define CONTROL_PIO_DATA_WIDTH 8
#define CONTROL_PIO_DO_TEST_BENCH_WIRING 0
#define CONTROL_PIO_DRIVEN_SIM_VALUE 0
#define CONTROL_PIO_EDGE_TYPE NONE
#define CONTROL_PIO_FREQ 50000000
#define CONTROL_PIO_HAS_IN 0
#define CONTROL_PIO_HAS_OUT 1
#define CONTROL_PIO_HAS_TRI 0
#define CONTROL_PIO_IRQ_TYPE NONE
#define CONTROL_PIO_RESET_VALUE 0


#endif /* _ALTERA_HPS_0_H_ */
