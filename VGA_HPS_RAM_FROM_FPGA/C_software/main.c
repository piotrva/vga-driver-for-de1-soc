#include <stdio.h>
#include <unistd.h>
#include <fcntl.h> 
#include <sys/mman.h> 
#include "hwlib.h" 
#include "socal/socal.h"
#include "socal/hps.h" 
#include "socal/alt_gpio.h"
#include "hps_0.h"


#define REG_BASE 0xFF200000
#define REG_SPAN 0x00200000

void* virtual_base;
void* addr_addr;
void* ctrl_addr;
int fd;

int i,j;

uint32_t x=0xABCDEF;

int main(){
	fd = open("/dev/mem", (O_RDWR|O_SYNC));
	virtual_base = mmap(NULL, REG_SPAN, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, REG_BASE);
	addr_addr = virtual_base + ADDRESS_PIO_BASE;
	ctrl_addr = virtual_base + CONTROL_PIO_BASE;
	*((uint32_t *)(addr_addr)) = (uint32_t)&x;
	*((uint32_t *)(ctrl_addr)) = 0xFF;
	while(1){
		x++;
		usleep(100000);
	}
	return 0;
}
