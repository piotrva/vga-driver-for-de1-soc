library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.decoder_7seg.all;


ENTITY HPSFPGA IS
	PORT(
---------FPGA Connections-------------
		CLOCK_50: IN STD_LOGIC;
		SW:IN STD_LOGIC_VECTOR(9 downto 0);
		LEDR: OUT STD_LOGIC_VECTOR(9 downto 0);
		VGA_R			: out std_logic_vector(7 downto 0);
		VGA_G			: out std_logic_vector(7 downto 0);
		VGA_B			: out std_logic_vector(7 downto 0);
		VGA_CLK		: out std_logic;
		VGA_BLANK_N	: out std_logic;
		VGA_HS		: buffer std_logic;
		VGA_VS		: buffer std_logic;
		VGA_SYNC_N	: out std_logic;
		HEX0			: out std_logic_vector(0 to 6);
		HEX1			: out std_logic_vector(0 to 6);
		HEX2			: out std_logic_vector(0 to 6);
		HEX3			: out std_logic_vector(0 to 6);
		HEX4			: out std_logic_vector(0 to 6);
		HEX5			: out std_logic_vector(0 to 6);
---------HPS Connections---------------
		HPS_CONV_USB_N:INOUT STD_LOGIC;
		HPS_DDR3_ADDR:OUT STD_LOGIC_VECTOR(14 downto 0);
		HPS_DDR3_BA: OUT STD_LOGIC_VECTOR(2 downto 0);
		HPS_DDR3_CAS_N: OUT STD_LOGIC;
		HPS_DDR3_CKE:OUT STD_LOGIC;
		HPS_DDR3_CK_N: OUT STD_LOGIC;
		HPS_DDR3_CK_P: OUT STD_LOGIC;
		HPS_DDR3_CS_N: OUT STD_LOGIC;
		HPS_DDR3_DM: OUT STD_LOGIC_VECTOR(3 downto 0);
		HPS_DDR3_DQ: INOUT STD_LOGIC_VECTOR(31 downto 0);
		HPS_DDR3_DQS_N: INOUT STD_LOGIC_VECTOR(3 downto 0);
		HPS_DDR3_DQS_P: INOUT STD_LOGIC_VECTOR(3 downto 0);
		HPS_DDR3_ODT: OUT STD_LOGIC;
		HPS_DDR3_RAS_N: OUT STD_LOGIC;
		HPS_DDR3_RESET_N: OUT  STD_LOGIC;
		HPS_DDR3_RZQ: IN  STD_LOGIC;
		HPS_DDR3_WE_N: OUT STD_LOGIC;
		HPS_ENET_GTX_CLK: OUT STD_LOGIC;
		HPS_ENET_INT_N:INOUT STD_LOGIC;
		HPS_ENET_MDC:OUT STD_LOGIC;
		HPS_ENET_MDIO:INOUT STD_LOGIC;
		HPS_ENET_RX_CLK: IN STD_LOGIC;
		HPS_ENET_RX_DATA: IN STD_LOGIC_VECTOR(3 downto 0);
		HPS_ENET_RX_DV: IN STD_LOGIC;
		HPS_ENET_TX_DATA: OUT STD_LOGIC_VECTOR(3 downto 0);
		HPS_ENET_TX_EN: OUT STD_LOGIC;
		HPS_KEY: INOUT STD_LOGIC;
		HPS_SD_CLK: OUT STD_LOGIC;
		HPS_SD_CMD: INOUT STD_LOGIC;
		HPS_SD_DATA: INOUT STD_LOGIC_VECTOR(3 downto 0);
		HPS_UART_RX: IN   STD_LOGIC;
		HPS_UART_TX: OUT STD_LOGIC;
		HPS_USB_CLKOUT: IN STD_LOGIC;
		HPS_USB_DATA:INOUT STD_LOGIC_VECTOR(7 downto 0);
		HPS_USB_DIR: IN STD_LOGIC;
		HPS_USB_NXT: IN STD_LOGIC;
		HPS_USB_STP: OUT STD_LOGIC

	);
END HPSFPGA;

ARCHITECTURE MAIN OF HPSFPGA IS
 component hps_fpga is
        port (
            clk_clk                         : in    std_logic                     := 'X';             -- clk
            reset_reset_n                   : in    std_logic                     := 'X';             -- reset_n
            memory_mem_a                    : out   std_logic_vector(14 downto 0);                    -- mem_a
            memory_mem_ba                   : out   std_logic_vector(2 downto 0);                     -- mem_ba
            memory_mem_ck                   : out   std_logic;                                        -- mem_ck
            memory_mem_ck_n                 : out   std_logic;                                        -- mem_ck_n
            memory_mem_cke                  : out   std_logic;                                        -- mem_cke
            memory_mem_cs_n                 : out   std_logic;                                        -- mem_cs_n
            memory_mem_ras_n                : out   std_logic;                                        -- mem_ras_n
            memory_mem_cas_n                : out   std_logic;                                        -- mem_cas_n
            memory_mem_we_n                 : out   std_logic;                                        -- mem_we_n
            memory_mem_reset_n              : out   std_logic;                                        -- mem_reset_n
            memory_mem_dq                   : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            memory_mem_dqs                  : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            memory_mem_dqs_n                : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            memory_mem_odt                  : out   std_logic;                                        -- mem_odt
            memory_mem_dm                   : out   std_logic_vector(3 downto 0);                     -- mem_dm
            memory_oct_rzqin                : in    std_logic                     := 'X';             -- oct_rzqin
            hps_0_h2f_reset_reset_n         : out   std_logic;                                        -- reset_n
            --led_external_connection_export  : out   std_logic_vector(9 downto 0);                     -- export
            --sw_external_connection_export   : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- export
            hps_io_hps_io_emac1_inst_TX_CLK : out   std_logic;                                        -- hps_io_emac1_inst_TX_CLK
            hps_io_hps_io_emac1_inst_TXD0   : out   std_logic;                                        -- hps_io_emac1_inst_TXD0
            hps_io_hps_io_emac1_inst_TXD1   : out   std_logic;                                        -- hps_io_emac1_inst_TXD1
            hps_io_hps_io_emac1_inst_TXD2   : out   std_logic;                                        -- hps_io_emac1_inst_TXD2
            hps_io_hps_io_emac1_inst_TXD3   : out   std_logic;                                        -- hps_io_emac1_inst_TXD3
            hps_io_hps_io_emac1_inst_RXD0   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD0
            hps_io_hps_io_emac1_inst_MDIO   : inout std_logic                     := 'X';             -- hps_io_emac1_inst_MDIO
            hps_io_hps_io_emac1_inst_MDC    : out   std_logic;                                        -- hps_io_emac1_inst_MDC
            hps_io_hps_io_emac1_inst_RX_CTL : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CTL
            hps_io_hps_io_emac1_inst_TX_CTL : out   std_logic;                                        -- hps_io_emac1_inst_TX_CTL
            hps_io_hps_io_emac1_inst_RX_CLK : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CLK
            hps_io_hps_io_emac1_inst_RXD1   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD1
            hps_io_hps_io_emac1_inst_RXD2   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD2
            hps_io_hps_io_emac1_inst_RXD3   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD3
            hps_io_hps_io_sdio_inst_CMD     : inout std_logic                     := 'X';             -- hps_io_sdio_inst_CMD
            hps_io_hps_io_sdio_inst_D0      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D0
            hps_io_hps_io_sdio_inst_D1      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D1
            hps_io_hps_io_sdio_inst_CLK     : out   std_logic;                                        -- hps_io_sdio_inst_CLK
            hps_io_hps_io_sdio_inst_D2      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D2
            hps_io_hps_io_sdio_inst_D3      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D3
            hps_io_hps_io_usb1_inst_D0      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D0
            hps_io_hps_io_usb1_inst_D1      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D1
            hps_io_hps_io_usb1_inst_D2      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D2
            hps_io_hps_io_usb1_inst_D3      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D3
            hps_io_hps_io_usb1_inst_D4      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D4
            hps_io_hps_io_usb1_inst_D5      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D5
            hps_io_hps_io_usb1_inst_D6      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D6
            hps_io_hps_io_usb1_inst_D7      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D7
            hps_io_hps_io_usb1_inst_CLK     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_CLK
            hps_io_hps_io_usb1_inst_STP     : out   std_logic;                                        -- hps_io_usb1_inst_STP
            hps_io_hps_io_usb1_inst_DIR     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_DIR
            hps_io_hps_io_usb1_inst_NXT     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_NXT
            hps_io_hps_io_uart0_inst_RX     : in    std_logic                     := 'X';             -- hps_io_uart0_inst_RX
            hps_io_hps_io_uart0_inst_TX     : out   std_logic;                                         -- hps_io_uart0_inst_TX
				address_ram_export                  : out   std_logic_vector(31 downto 0);                    -- export
				control_export                      : out   std_logic_vector(7 downto 0);                      -- export
				hps_0_f2h_sdram0_clock_clk          : in    std_logic                     := 'X';             -- clk
            hps_0_f2h_sdram0_data_address       : in    std_logic_vector(29 downto 0) := (others => 'X'); -- address
            hps_0_f2h_sdram0_data_burstcount    : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- burstcount
            hps_0_f2h_sdram0_data_waitrequest   : out   std_logic;                                        -- waitrequest
            hps_0_f2h_sdram0_data_readdata      : out   std_logic_vector(31 downto 0);                    -- readdata
            hps_0_f2h_sdram0_data_readdatavalid : out   std_logic;                                        -- readdatavalid
            hps_0_f2h_sdram0_data_read          : in    std_logic                     := 'X'              -- read

        );
    end component hps_fpga;
	 
	 component PLL_VGA is
		port (
			refclk   : in  std_logic := '0'; --  refclk.clk 50M (0 deg)
			rst      : in  std_logic := '0'; --   reset.reset
			outclk_0 : out std_logic;        -- outclk0.clk 65M (0 deg)
			outclk_1 : out std_logic;        -- outclk1.clk 65M (180 deg)
			locked   : out std_logic         --  locked.export
		);
	end component PLL_VGA;
	
SIGNAL HPS_H2F_RST:STD_LOGIC;
signal vid_datavalid : std_logic;

signal CLOCK_65 : std_logic;
signal CLOCK_65_N:std_logic;

signal sdram0_clock_clk:std_logic := '0';
signal sdram0_data_address:std_logic_vector(29 downto 0);
signal sdram0_data_readdata:std_logic_vector(31 downto 0);
signal sdram0_data_readdatavalid:std_logic;
signal sdram0_data_read:std_logic;
signal sdram0_data_waitrequest:std_logic;

signal address_ram                  :std_logic_vector(31 downto 0);                    -- export
signal control                     :std_logic_vector(7 downto 0);                      -- export

signal rd_data : std_logic_vector(23 downto 0) := (others => '0');

type mem_rd_state_type is (MEM_IDLE, MEM_ADDR_HOLD, MEM_RELEASE_RD, MEM_VALID_WAIT);
signal mem_rd_state : mem_rd_state_type := MEM_IDLE;
	
BEGIN

LED01 : seg7 port map(s => HEX0, q => rd_data(3 downto 0));
LED02 : seg7 port map(s => HEX1, q => rd_data(7 downto 4));
LED03 : seg7 port map(s => HEX2, q => rd_data(11 downto 8));
LED04 : seg7 port map(s => HEX3, q => rd_data(15 downto 12));
LED05 : seg7 port map(s => HEX4, q => rd_data(19 downto 16));
LED06 : seg7 port map(s => HEX5, q => rd_data(23 downto 20));

VGA_BLANK_N <= '1';
VGA_SYNC_N <= '0';



PLL01 : component PLL_VGA
		port map (
			refclk   => CLOCK_50,   --  refclk.clk
			rst      => '0',      --   reset.reset
			outclk_0 => CLOCK_65, -- outclk0.clk
			outclk_1 => CLOCK_65_N, -- outclk1.clk
			locked   => open    --  locked.export
		);

VGA_CLK <= CLOCK_65;

process(CLOCK_65)
begin
if(sdram0_clock_clk='0')then
	sdram0_clock_clk <= not sdram0_clock_clk;
		case mem_rd_state is
			when MEM_IDLE =>
				if(control(0)='1')then
					sdram0_data_address <= address_ram(29 downto 0);--apply address
					sdram0_data_read <= '1';
					mem_rd_state <= MEM_ADDR_HOLD;
				end if;
			when MEM_ADDR_HOLD =>
				mem_rd_state <= MEM_ADDR_HOLD;
			when MEM_RELEASE_RD =>
				sdram0_data_read <= '0';
				mem_rd_state <= MEM_VALID_WAIT;
			when MEM_VALID_WAIT =>
				mem_rd_state <= MEM_VALID_WAIT;
		end case;
else
	sdram0_clock_clk <= not sdram0_clock_clk;
		case mem_rd_state is
			when MEM_IDLE =>
				mem_rd_state <= MEM_IDLE;
			when MEM_ADDR_HOLD =>
				if(sdram0_data_waitrequest='0')then
					mem_rd_state <= MEM_RELEASE_RD;
				else
					mem_rd_state <= MEM_ADDR_HOLD;
				end if;
			when MEM_RELEASE_RD =>
				mem_rd_state <= MEM_RELEASE_RD;
			when MEM_VALID_WAIT =>
				if(sdram0_data_readdatavalid='1')then
					mem_rd_state <= MEM_IDLE;
					rd_data <= sdram0_data_readdata(23 downto 0);
				else
					mem_rd_state <= MEM_VALID_WAIT;
				end if;
		end case;
end if;
end process;

		
u0 : component hps_fpga
        port map (
            clk_clk                         => CLOCK_50,                         --                     clk.clk
            reset_reset_n                   => '1',                   --                   reset.reset_n
            memory_mem_a                    => HPS_DDR3_ADDR,                    --                  memory.mem_a
            memory_mem_ba                   => HPS_DDR3_BA,                   --                        .mem_ba
            memory_mem_ck                   => HPS_DDR3_CK_P,                   --                        .mem_ck
            memory_mem_ck_n                 => HPS_DDR3_CK_N,                 --                        .mem_ck_n
            memory_mem_cke                  => HPS_DDR3_CKE,                  --                        .mem_cke
            memory_mem_cs_n                 => HPS_DDR3_CS_N,                 --                        .mem_cs_n
            memory_mem_ras_n                => HPS_DDR3_RAS_N,                --                        .mem_ras_n
            memory_mem_cas_n                => HPS_DDR3_CAS_N,                --                        .mem_cas_n
            memory_mem_we_n                 => HPS_DDR3_WE_N,                 --                        .mem_we_n
            memory_mem_reset_n              => HPS_DDR3_RESET_N,              --                        .mem_reset_n
            memory_mem_dq                   => HPS_DDR3_DQ,                   --                        .mem_dq
            memory_mem_dqs                  => HPS_DDR3_DQS_P,                  --                        .mem_dqs
            memory_mem_dqs_n                => HPS_DDR3_DQS_N,                --                        .mem_dqs_n
            memory_mem_odt                  => HPS_DDR3_ODT,                  --                        .mem_odt
            memory_mem_dm                   => HPS_DDR3_DM,                   --                        .mem_dm
            memory_oct_rzqin                => HPS_DDR3_RZQ,                --                        .oct_rzqin
            hps_0_h2f_reset_reset_n         => HPS_H2F_RST,         --         hps_0_h2f_reset.reset_n
            --led_external_connection_export  => LEDR,  -- led_external_connection.export
            --sw_external_connection_export   => SW,   --  sw_external_connection.export
            hps_io_hps_io_emac1_inst_TX_CLK => HPS_ENET_GTX_CLK, --                  hps_io.hps_io_emac1_inst_TX_CLK
            hps_io_hps_io_emac1_inst_TXD0   => HPS_ENET_TX_DATA(0),   --                        .hps_io_emac1_inst_TXD0
            hps_io_hps_io_emac1_inst_TXD1   => HPS_ENET_TX_DATA(1),   --                        .hps_io_emac1_inst_TXD1
            hps_io_hps_io_emac1_inst_TXD2   => HPS_ENET_TX_DATA(2),   --                        .hps_io_emac1_inst_TXD2
            hps_io_hps_io_emac1_inst_TXD3   => HPS_ENET_TX_DATA(3),   --                        .hps_io_emac1_inst_TXD3
            hps_io_hps_io_emac1_inst_RXD0   => HPS_ENET_RX_DATA(0),   --                        .hps_io_emac1_inst_RXD0
            hps_io_hps_io_emac1_inst_MDIO   => HPS_ENET_MDIO,   --                        .hps_io_emac1_inst_MDIO
            hps_io_hps_io_emac1_inst_MDC    => HPS_ENET_MDC,    --                        .hps_io_emac1_inst_MDC
            hps_io_hps_io_emac1_inst_RX_CTL => HPS_ENET_RX_DV, --                        .hps_io_emac1_inst_RX_CTL
            hps_io_hps_io_emac1_inst_TX_CTL => HPS_ENET_TX_EN, --                        .hps_io_emac1_inst_TX_CTL
            hps_io_hps_io_emac1_inst_RX_CLK => HPS_ENET_RX_CLK, --                        .hps_io_emac1_inst_RX_CLK
            hps_io_hps_io_emac1_inst_RXD1   => HPS_ENET_RX_DATA(1),   --                        .hps_io_emac1_inst_RXD1
            hps_io_hps_io_emac1_inst_RXD2   => HPS_ENET_RX_DATA(2),   --                        .hps_io_emac1_inst_RXD2
            hps_io_hps_io_emac1_inst_RXD3   => HPS_ENET_RX_DATA(3),   --                        .hps_io_emac1_inst_RXD3
            hps_io_hps_io_sdio_inst_CMD     => HPS_SD_CMD,     --                        .hps_io_sdio_inst_CMD
            hps_io_hps_io_sdio_inst_D0      => HPS_SD_DATA(0),      --                        .hps_io_sdio_inst_D0
            hps_io_hps_io_sdio_inst_D1      => HPS_SD_DATA(1),      --                        .hps_io_sdio_inst_D1
            hps_io_hps_io_sdio_inst_CLK     => HPS_SD_CLK,     --                        .hps_io_sdio_inst_CLK
            hps_io_hps_io_sdio_inst_D2      => HPS_SD_DATA(2),      --                        .hps_io_sdio_inst_D2
            hps_io_hps_io_sdio_inst_D3      => HPS_SD_DATA(3),      --                        .hps_io_sdio_inst_D3
            hps_io_hps_io_usb1_inst_D0      => HPS_USB_DATA(0),      --                        .hps_io_usb1_inst_D0
            hps_io_hps_io_usb1_inst_D1      => HPS_USB_DATA(1),      --                        .hps_io_usb1_inst_D1
            hps_io_hps_io_usb1_inst_D2      => HPS_USB_DATA(2),      --                        .hps_io_usb1_inst_D2
            hps_io_hps_io_usb1_inst_D3      => HPS_USB_DATA(3),      --                        .hps_io_usb1_inst_D3
            hps_io_hps_io_usb1_inst_D4      => HPS_USB_DATA(4),      --                        .hps_io_usb1_inst_D4
            hps_io_hps_io_usb1_inst_D5      => HPS_USB_DATA(5),      --                        .hps_io_usb1_inst_D5
            hps_io_hps_io_usb1_inst_D6      => HPS_USB_DATA(6),      --                        .hps_io_usb1_inst_D6
            hps_io_hps_io_usb1_inst_D7      => HPS_USB_DATA(7),      --                        .hps_io_usb1_inst_D7
            hps_io_hps_io_usb1_inst_CLK     => HPS_USB_CLKOUT,     --                        .hps_io_usb1_inst_CLK
            hps_io_hps_io_usb1_inst_STP     => HPS_USB_STP,     --                        .hps_io_usb1_inst_STP
            hps_io_hps_io_usb1_inst_DIR     => HPS_USB_DIR,     --                        .hps_io_usb1_inst_DIR
            hps_io_hps_io_usb1_inst_NXT     => HPS_USB_NXT,     --                        .hps_io_usb1_inst_NXT
            hps_io_hps_io_uart0_inst_RX     => HPS_UART_RX,     --                        .hps_io_uart0_inst_RX
            hps_io_hps_io_uart0_inst_TX     => HPS_UART_TX,      --                        .hps_io_uart0_inst_TX
				control_export                      => control,                       --                control.export
				address_ram_export                  => address_ram,                  --            address_ram.export
				hps_0_f2h_sdram0_clock_clk          => sdram0_clock_clk,          -- hps_0_f2h_sdram0_clock.clk
            hps_0_f2h_sdram0_data_address       => sdram0_data_address,       --  hps_0_f2h_sdram0_data.address
            hps_0_f2h_sdram0_data_burstcount    => "00000001",    --                       .burstcount
            hps_0_f2h_sdram0_data_waitrequest   => sdram0_data_waitrequest,   --                       .waitrequest
            hps_0_f2h_sdram0_data_readdata      => sdram0_data_readdata,      --                       .readdata
            hps_0_f2h_sdram0_data_readdatavalid => sdram0_data_readdatavalid, --                       .readdatavalid
            hps_0_f2h_sdram0_data_read          => sdram0_data_read           --                       .read
        );

END MAIN;