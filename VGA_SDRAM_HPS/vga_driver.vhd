library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


package vga_driver_pkg is

component vga_driver is
	generic(
		px_width				: integer := 800;
		px_front_porch		: integer := 40;
		px_sync_pulse		: integer := 128;
		px_back_porch		: integer := 88;
		line_height			: integer := 600;
		line_front_porch	: integer := 1;
		line_sync_pulse	: integer := 4;
		line_back_porch	: integer := 23
	);
	port(
		VGA_R			: out std_logic_vector(7 downto 0);
		VGA_G			: out std_logic_vector(7 downto 0);
		VGA_B			: out std_logic_vector(7 downto 0);
		VGA_IN		: in std_logic_vector(23 downto 0);
		VGA_CLK		: out std_logic;
		VGA_BLANK_N	: out std_logic;
		VGA_HS		: buffer std_logic;
		VGA_VS		: buffer std_logic;
		VGA_SYNC_N	: out std_logic;
		PX_ADDR		: buffer std_logic_vector(20 downto 0);
		CLK_PIX		: in std_logic;
		RST			: in std_logic
	);
end component vga_driver;

end package vga_driver_pkg;


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity vga_driver is
	generic(
		px_width				: integer := 800;
		px_front_porch		: integer := 40;
		px_sync_pulse		: integer := 128;
		px_back_porch		: integer := 88;
		line_height			: integer := 600;
		line_front_porch	: integer := 1;
		line_sync_pulse	: integer := 4;
		line_back_porch	: integer := 23
	);
	port(
		VGA_R			: out std_logic_vector(7 downto 0);
		VGA_G			: out std_logic_vector(7 downto 0);
		VGA_B			: out std_logic_vector(7 downto 0);
		VGA_IN		: in std_logic_vector(23 downto 0);
		VGA_CLK		: out std_logic;
		VGA_BLANK_N	: out std_logic;
		VGA_HS		: buffer std_logic;
		VGA_VS		: buffer std_logic;
		VGA_SYNC_N	: out std_logic;
		PX_ADDR		: buffer std_logic_vector(20 downto 0);
		CLK_PIX		: in std_logic;
		RST			: in std_logic
	);
end vga_driver;

architecture basic of vga_driver is
	signal px_counter : integer := 0;
	signal line_counter : integer := 0;
begin
	
	VGA_CLK <= clk_pix;
	VGA_SYNC_N <= not (VGA_HS xor VGA_VS);
	
	process(CLK_PIX, RST)
	begin
		if(RST='0') then
			px_counter <= 0;
			line_counter <= 0;
			PX_ADDR <= (others => '0');
		elsif(falling_edge(CLK_PIX)) then
			
			if(px_counter<px_width) then--data
				if(line_counter<line_height) then
					VGA_R <= VGA_IN(23 downto 16);
					VGA_G <= VGA_IN(15 downto 8);
					VGA_B <= VGA_IN(7 downto 0);
					PX_ADDR <= PX_ADDR + 1;
					VGA_BLANK_N <= '1';
					VGA_HS <= '1';
				else
					VGA_BLANK_N <= '0';
					VGA_HS <= '1';
				end if;
				px_counter <= px_counter + 1;
			elsif(px_counter<(px_width+px_front_porch)) then--front porch
				VGA_BLANK_N <= '0';
				px_counter <= px_counter + 1;
			elsif(px_counter<(px_width+px_front_porch+px_sync_pulse)) then--HS
				VGA_HS <= '0';
				px_counter <= px_counter + 1;
			elsif(px_counter<(px_width+px_front_porch+px_sync_pulse+px_back_porch-1)) then--back porch
				VGA_HS <= '1';
				px_counter <= px_counter + 1;
			else
				px_counter <= 0;
				if(line_counter<line_height)then--video
					line_counter <= line_counter + 1;
					VGA_VS <= '1';
				elsif(line_counter<(line_height+line_front_porch))then--front porch
					line_counter <= line_counter + 1;
				elsif(line_counter<(line_height+line_front_porch+line_sync_pulse))then--vs
					line_counter <= line_counter + 1;
					VGA_VS <= '0';
				elsif(line_counter<(line_height+line_front_porch+line_sync_pulse+line_back_porch-1))then--back porch
					line_counter <= line_counter + 1;
					VGA_VS <= '1';
				else
					line_counter <= 0;
					PX_ADDR <= (others => '0');
				end if;
			end if;
			
			
		end if;
	end process;
end basic;
