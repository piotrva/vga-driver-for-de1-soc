#ifndef _ALTERA_HPS_0_H_
#define _ALTERA_HPS_0_H_

/*
 * This file was automatically generated by the swinfo2header utility.
 * 
 * Created from SOPC Builder system 'hps_fpga' in
 * file './hps_fpga.sopcinfo'.
 */

/*
 * This file contains macros for module 'hps_0' and devices
 * connected to the following masters:
 *   h2f_axi_master
 *   h2f_lw_axi_master
 * 
 * Do not include this header file and another header file created for a
 * different module or master group at the same time.
 * Doing so may result in duplicate macro names.
 * Instead, use the system header file which has macros with unique names.
 */

/*
 * Macros for device 'sw', class 'altera_avalon_pio'
 * The macros are prefixed with 'SW_'.
 * The prefix is the slave descriptor.
 */
#define SW_COMPONENT_TYPE altera_avalon_pio
#define SW_COMPONENT_NAME sw
#define SW_BASE 0x0
#define SW_SPAN 32
#define SW_END 0x1f
#define SW_BIT_CLEARING_EDGE_REGISTER 0
#define SW_BIT_MODIFYING_OUTPUT_REGISTER 0
#define SW_CAPTURE 0
#define SW_DATA_WIDTH 10
#define SW_DO_TEST_BENCH_WIRING 0
#define SW_DRIVEN_SIM_VALUE 0
#define SW_EDGE_TYPE NONE
#define SW_FREQ 50000000
#define SW_HAS_IN 1
#define SW_HAS_OUT 0
#define SW_HAS_TRI 0
#define SW_IRQ_TYPE NONE
#define SW_RESET_VALUE 0

/*
 * Macros for device 'led', class 'altera_avalon_pio'
 * The macros are prefixed with 'LED_'.
 * The prefix is the slave descriptor.
 */
#define LED_COMPONENT_TYPE altera_avalon_pio
#define LED_COMPONENT_NAME led
#define LED_BASE 0x10
#define LED_SPAN 32
#define LED_END 0x2f
#define LED_BIT_CLEARING_EDGE_REGISTER 0
#define LED_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LED_CAPTURE 0
#define LED_DATA_WIDTH 10
#define LED_DO_TEST_BENCH_WIRING 0
#define LED_DRIVEN_SIM_VALUE 0
#define LED_EDGE_TYPE NONE
#define LED_FREQ 50000000
#define LED_HAS_IN 0
#define LED_HAS_OUT 1
#define LED_HAS_TRI 0
#define LED_IRQ_TYPE NONE
#define LED_RESET_VALUE 1023

/*
 * Macros for device 'ram_addr', class 'altera_avalon_pio'
 * The macros are prefixed with 'RAM_ADDR_'.
 * The prefix is the slave descriptor.
 */
#define RAM_ADDR_COMPONENT_TYPE altera_avalon_pio
#define RAM_ADDR_COMPONENT_NAME ram_addr
#define RAM_ADDR_BASE 0x20
#define RAM_ADDR_SPAN 32
#define RAM_ADDR_END 0x3f
#define RAM_ADDR_BIT_CLEARING_EDGE_REGISTER 0
#define RAM_ADDR_BIT_MODIFYING_OUTPUT_REGISTER 0
#define RAM_ADDR_CAPTURE 0
#define RAM_ADDR_DATA_WIDTH 25
#define RAM_ADDR_DO_TEST_BENCH_WIRING 0
#define RAM_ADDR_DRIVEN_SIM_VALUE 0
#define RAM_ADDR_EDGE_TYPE NONE
#define RAM_ADDR_FREQ 50000000
#define RAM_ADDR_HAS_IN 0
#define RAM_ADDR_HAS_OUT 1
#define RAM_ADDR_HAS_TRI 0
#define RAM_ADDR_IRQ_TYPE NONE
#define RAM_ADDR_RESET_VALUE 0

/*
 * Macros for device 'ram_data_wr', class 'altera_avalon_pio'
 * The macros are prefixed with 'RAM_DATA_WR_'.
 * The prefix is the slave descriptor.
 */
#define RAM_DATA_WR_COMPONENT_TYPE altera_avalon_pio
#define RAM_DATA_WR_COMPONENT_NAME ram_data_wr
#define RAM_DATA_WR_BASE 0x30
#define RAM_DATA_WR_SPAN 32
#define RAM_DATA_WR_END 0x4f
#define RAM_DATA_WR_BIT_CLEARING_EDGE_REGISTER 0
#define RAM_DATA_WR_BIT_MODIFYING_OUTPUT_REGISTER 0
#define RAM_DATA_WR_CAPTURE 0
#define RAM_DATA_WR_DATA_WIDTH 17
#define RAM_DATA_WR_DO_TEST_BENCH_WIRING 0
#define RAM_DATA_WR_DRIVEN_SIM_VALUE 0
#define RAM_DATA_WR_EDGE_TYPE NONE
#define RAM_DATA_WR_FREQ 50000000
#define RAM_DATA_WR_HAS_IN 0
#define RAM_DATA_WR_HAS_OUT 1
#define RAM_DATA_WR_HAS_TRI 0
#define RAM_DATA_WR_IRQ_TYPE NONE
#define RAM_DATA_WR_RESET_VALUE 0

/*
 * Macros for device 'ram_wr_done', class 'altera_avalon_pio'
 * The macros are prefixed with 'RAM_WR_DONE_'.
 * The prefix is the slave descriptor.
 */
#define RAM_WR_DONE_COMPONENT_TYPE altera_avalon_pio
#define RAM_WR_DONE_COMPONENT_NAME ram_wr_done
#define RAM_WR_DONE_BASE 0x40
#define RAM_WR_DONE_SPAN 32
#define RAM_WR_DONE_END 0x5f
#define RAM_WR_DONE_BIT_CLEARING_EDGE_REGISTER 0
#define RAM_WR_DONE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define RAM_WR_DONE_CAPTURE 0
#define RAM_WR_DONE_DATA_WIDTH 1
#define RAM_WR_DONE_DO_TEST_BENCH_WIRING 0
#define RAM_WR_DONE_DRIVEN_SIM_VALUE 0
#define RAM_WR_DONE_EDGE_TYPE NONE
#define RAM_WR_DONE_FREQ 50000000
#define RAM_WR_DONE_HAS_IN 1
#define RAM_WR_DONE_HAS_OUT 0
#define RAM_WR_DONE_HAS_TRI 0
#define RAM_WR_DONE_IRQ_TYPE NONE
#define RAM_WR_DONE_RESET_VALUE 0


#endif /* _ALTERA_HPS_0_H_ */
