library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.vga_driver_pkg.all;
use WORK.common.all;
use WORK.sdram.all;
use WORK.XSASDRAM.all;



ENTITY HPSFPGA IS
PORT(
	---------FPGA Connections-------------
	CLOCK_50: IN STD_LOGIC;
	SW:IN STD_LOGIC_VECTOR(9 downto 0);
	LEDR: OUT STD_LOGIC_VECTOR(9 downto 0);
	--VGA
	VGA_R			: out std_logic_vector(7 downto 0);
	VGA_G			: out std_logic_vector(7 downto 0);
	VGA_B			: out std_logic_vector(7 downto 0);
	VGA_CLK		: out std_logic;
	VGA_BLANK_N	: out std_logic;
	VGA_HS		: buffer std_logic;
	VGA_VS		: buffer std_logic;
	VGA_SYNC_N	: out std_logic;
	--SDRAM connections
		DRAM_ADDR	: out std_logic_vector(12 downto 0);
		DRAM_DQ		: inout std_logic_vector(15 downto 0);
		DRAM_BA		: out std_logic_vector(1 downto 0);
		DRAM_LDQM 	: out std_logic;
		DRAM_UDQM	: out std_logic;
		DRAM_RAS_N	: out std_logic;
		DRAM_CAS_N	: out std_logic;
		DRAM_CKE		: out std_logic;
		DRAM_CLK		: out std_logic;
		DRAM_WE_N	: out std_logic;
		DRAM_CS_N	: out std_logic;
	---------HPS Connections---------------
	HPS_CONV_USB_N:INOUT STD_LOGIC;
	HPS_DDR3_ADDR:OUT STD_LOGIC_VECTOR(14 downto 0);
	HPS_DDR3_BA: OUT STD_LOGIC_VECTOR(2 downto 0);
	HPS_DDR3_CAS_N: OUT STD_LOGIC;
	HPS_DDR3_CKE:OUT STD_LOGIC;
	HPS_DDR3_CK_N: OUT STD_LOGIC;
	HPS_DDR3_CK_P: OUT STD_LOGIC;
	HPS_DDR3_CS_N: OUT STD_LOGIC;
	HPS_DDR3_DM: OUT STD_LOGIC_VECTOR(3 downto 0);
	HPS_DDR3_DQ: INOUT STD_LOGIC_VECTOR(31 downto 0);
	HPS_DDR3_DQS_N: INOUT STD_LOGIC_VECTOR(3 downto 0);
	HPS_DDR3_DQS_P: INOUT STD_LOGIC_VECTOR(3 downto 0);
	HPS_DDR3_ODT: OUT STD_LOGIC;
	HPS_DDR3_RAS_N: OUT STD_LOGIC;
	HPS_DDR3_RESET_N: OUT  STD_LOGIC;
	HPS_DDR3_RZQ: IN  STD_LOGIC;
	HPS_DDR3_WE_N: OUT STD_LOGIC;
	HPS_ENET_GTX_CLK: OUT STD_LOGIC;
	HPS_ENET_INT_N:INOUT STD_LOGIC;
	HPS_ENET_MDC:OUT STD_LOGIC;
	HPS_ENET_MDIO:INOUT STD_LOGIC;
	HPS_ENET_RX_CLK: IN STD_LOGIC;
	HPS_ENET_RX_DATA: IN STD_LOGIC_VECTOR(3 downto 0);
	HPS_ENET_RX_DV: IN STD_LOGIC;
	HPS_ENET_TX_DATA: OUT STD_LOGIC_VECTOR(3 downto 0);
	HPS_ENET_TX_EN: OUT STD_LOGIC;
	HPS_KEY: INOUT STD_LOGIC;
	HPS_SD_CLK: OUT STD_LOGIC;
	HPS_SD_CMD: INOUT STD_LOGIC;
	HPS_SD_DATA: INOUT STD_LOGIC_VECTOR(3 downto 0);
	HPS_UART_RX: IN   STD_LOGIC;
	HPS_UART_TX: OUT STD_LOGIC;
	HPS_USB_CLKOUT: IN STD_LOGIC;
	HPS_USB_DATA:INOUT STD_LOGIC_VECTOR(7 downto 0);
	HPS_USB_DIR: IN STD_LOGIC;
	HPS_USB_NXT: IN STD_LOGIC;
	HPS_USB_STP: OUT STD_LOGIC

);
END HPSFPGA;

ARCHITECTURE MAIN OF HPSFPGA IS
 component hps_fpga is
        port (
            clk_clk                         : in    std_logic                     := 'X';             -- clk
            reset_reset_n                   : in    std_logic                     := 'X';             -- reset_n
            memory_mem_a                    : out   std_logic_vector(14 downto 0);                    -- mem_a
            memory_mem_ba                   : out   std_logic_vector(2 downto 0);                     -- mem_ba
            memory_mem_ck                   : out   std_logic;                                        -- mem_ck
            memory_mem_ck_n                 : out   std_logic;                                        -- mem_ck_n
            memory_mem_cke                  : out   std_logic;                                        -- mem_cke
            memory_mem_cs_n                 : out   std_logic;                                        -- mem_cs_n
            memory_mem_ras_n                : out   std_logic;                                        -- mem_ras_n
            memory_mem_cas_n                : out   std_logic;                                        -- mem_cas_n
            memory_mem_we_n                 : out   std_logic;                                        -- mem_we_n
            memory_mem_reset_n              : out   std_logic;                                        -- mem_reset_n
            memory_mem_dq                   : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
            memory_mem_dqs                  : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
            memory_mem_dqs_n                : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
            memory_mem_odt                  : out   std_logic;                                        -- mem_odt
            memory_mem_dm                   : out   std_logic_vector(3 downto 0);                     -- mem_dm
            memory_oct_rzqin                : in    std_logic                     := 'X';             -- oct_rzqin
            hps_0_h2f_reset_reset_n         : out   std_logic;                                        -- reset_n
            led_external_connection_export  : out   std_logic_vector(9 downto 0);                     -- export
            sw_external_connection_export   : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- export
            hps_io_hps_io_emac1_inst_TX_CLK : out   std_logic;                                        -- hps_io_emac1_inst_TX_CLK
            hps_io_hps_io_emac1_inst_TXD0   : out   std_logic;                                        -- hps_io_emac1_inst_TXD0
            hps_io_hps_io_emac1_inst_TXD1   : out   std_logic;                                        -- hps_io_emac1_inst_TXD1
            hps_io_hps_io_emac1_inst_TXD2   : out   std_logic;                                        -- hps_io_emac1_inst_TXD2
            hps_io_hps_io_emac1_inst_TXD3   : out   std_logic;                                        -- hps_io_emac1_inst_TXD3
            hps_io_hps_io_emac1_inst_RXD0   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD0
            hps_io_hps_io_emac1_inst_MDIO   : inout std_logic                     := 'X';             -- hps_io_emac1_inst_MDIO
            hps_io_hps_io_emac1_inst_MDC    : out   std_logic;                                        -- hps_io_emac1_inst_MDC
            hps_io_hps_io_emac1_inst_RX_CTL : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CTL
            hps_io_hps_io_emac1_inst_TX_CTL : out   std_logic;                                        -- hps_io_emac1_inst_TX_CTL
            hps_io_hps_io_emac1_inst_RX_CLK : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CLK
            hps_io_hps_io_emac1_inst_RXD1   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD1
            hps_io_hps_io_emac1_inst_RXD2   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD2
            hps_io_hps_io_emac1_inst_RXD3   : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD3
            hps_io_hps_io_sdio_inst_CMD     : inout std_logic                     := 'X';             -- hps_io_sdio_inst_CMD
            hps_io_hps_io_sdio_inst_D0      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D0
            hps_io_hps_io_sdio_inst_D1      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D1
            hps_io_hps_io_sdio_inst_CLK     : out   std_logic;                                        -- hps_io_sdio_inst_CLK
            hps_io_hps_io_sdio_inst_D2      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D2
            hps_io_hps_io_sdio_inst_D3      : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D3
            hps_io_hps_io_usb1_inst_D0      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D0
            hps_io_hps_io_usb1_inst_D1      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D1
            hps_io_hps_io_usb1_inst_D2      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D2
            hps_io_hps_io_usb1_inst_D3      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D3
            hps_io_hps_io_usb1_inst_D4      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D4
            hps_io_hps_io_usb1_inst_D5      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D5
            hps_io_hps_io_usb1_inst_D6      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D6
            hps_io_hps_io_usb1_inst_D7      : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D7
            hps_io_hps_io_usb1_inst_CLK     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_CLK
            hps_io_hps_io_usb1_inst_STP     : out   std_logic;                                        -- hps_io_usb1_inst_STP
            hps_io_hps_io_usb1_inst_DIR     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_DIR
            hps_io_hps_io_usb1_inst_NXT     : in    std_logic                     := 'X';             -- hps_io_usb1_inst_NXT
            hps_io_hps_io_uart0_inst_RX     : in    std_logic                     := 'X';             -- hps_io_uart0_inst_RX
            hps_io_hps_io_uart0_inst_TX     : out   std_logic;                                         -- hps_io_uart0_inst_TX
				ram_addr_external_connection_export    : out   std_logic_vector(24 downto 0);                    -- export
            ram_data_wr_external_connection_export : out   std_logic_vector(16 downto 0);                    -- export
            ram_wr_done_external_connection_export : in    std_logic
        );
    end component hps_fpga;
	 
	 component PLL_VGA_40M is
		port (
			refclk   : in  std_logic := '0'; --  refclk.clk 50
			rst      : in  std_logic := '0'; --   reset.reset
			outclk_0 : out std_logic;        -- outclk0.clk 40
			outclk_1 : out std_logic;        -- outclk1.clk 100
			outclk_2 : out std_logic;        -- outclk2.clk 200
			locked   : out std_logic         --  locked.export
		);
	end component PLL_VGA_40M;
	
	component PLL_25_175 is
		port (
			refclk   : in  std_logic := '0'; --  refclk.clk 50
			rst      : in  std_logic := '0'; --   reset.reset
			outclk_0 : out std_logic;        -- outclk0.clk 25.175
			locked   : out std_logic         --  locked.export
		);
	end component PLL_25_175;
	 
SIGNAL HPS_H2F_RST:STD_LOGIC;
signal CLOCK_40M : std_logic;
signal CLOCK_100M: std_logic;
signal CLOCK_200M: std_logic;
signal CLOCK_25_175M : std_logic;

signal pix_data : std_logic_vector(23 downto 0) := (others => '0');
signal PX_ADDR : std_logic_vector(20 downto 0);

signal SW_RAM_ADDR	: std_logic_vector(24 downto 0);
signal SW_RAM_DATA	: std_logic_vector(15 downto 0);
signal SW_RAM_WR		: std_logic;
signal SW_RAM_WR_DONE: std_logic;

signal rst_sdram				: std_logic;
signal rd_sdram				: std_logic;
signal wr_sdram				: std_logic;
signal earlyOpBegun_sdram	: std_logic;
signal opBegun_sdram			: std_logic;
signal rdPending_sdram		: std_logic;
signal done_sdram				: std_logic;
signal rdDone_sdram			: std_logic;
signal hAddr_sdram			: std_logic_vector(24 downto 0);
signal hDIn_sdram				: std_logic_vector(15 downto 0);
signal hDOut_sdram			: std_logic_vector(15 downto 0);
signal status_sdram			: std_logic_vector(3 downto 0);

signal rd_vga					: std_logic;
signal rdDone_vga				: std_logic;
signal hAddr_vga				: std_logic_vector(24 downto 0) := (others => '0');
signal hDOut_vga				: std_logic_vector(15 downto 0);

type fsm_sdram_reader_type is (ADDR_LATCH, RD_APPLY, DONE_WAIT);
signal fsm_sdram_reader : fsm_sdram_reader_type := ADDR_LATCH;
	
BEGIN

PLL1: PLL_VGA_40M port map(
	refclk => CLOCK_50,
	rst => '0',
	outclk_0 => CLOCK_40M,
	outclk_1 => CLOCK_100M,
	outclk_2 => CLOCK_200M
	);
	
PLL2: PLL_25_175 port map(
	refclk => CLOCK_50,
	rst => '0',
	outclk_0 => CLOCK_25_175M
	);

	
VGA1 : vga_driver
	generic map(
		px_width				=> 640,
		px_front_porch		=> 16,
		px_sync_pulse		=> 96,
		px_back_porch		=> 48,
		line_height			=> 480,
		line_front_porch	=> 11,
		line_sync_pulse	=> 2,
		line_back_porch	=> 31
		
	) 
	port map(
		VGA_R => VGA_R,
		VGA_G => VGA_G,
		VGA_B => VGA_B,
		VGA_IN => pix_data,
		VGA_CLK => VGA_CLK,
		VGA_BLANK_N => VGA_BLANK_N,
		VGA_HS => VGA_HS,
		VGA_VS => VGA_VS,
		VGA_SYNC_N => VGA_SYNC_N,
		CLK_PIX => CLOCK_25_175M,
		PX_ADDR => PX_ADDR,
		RST => SW(0)
	);

	--pix_data <= SW_RAM_ADDR(23 downto 0);
	
	process(CLOCK_200M)
	begin
		if(rising_edge(CLOCK_200M)) then
			case fsm_sdram_reader is
				
				when ADDR_LATCH =>
					hAddr_vga(20 downto 2) <= PX_ADDR(20 downto 2);
					hAddr_vga(24 downto 21) <= (others => '0');
					hAddr_vga(1 downto 0) <= (others => '0');
					fsm_sdram_reader <= RD_APPLY;
				when RD_APPLY =>
					rd_vga <= '1';
					fsm_sdram_reader <= DONE_WAIT;
				when DONE_WAIT =>
					if(rdDone_vga='1') then
						rd_vga <= '0';
						fsm_sdram_reader <= ADDR_LATCH;
						pix_data(7 downto 3) <= hDOut_vga(4 downto 0);
						pix_data(15 downto 11) <= hDOut_vga(9 downto 5);
						pix_data(23 downto 19) <= hDOut_vga(14 downto 10);
					end if;
				
				when others =>
					rd_vga <= '0';
					fsm_sdram_reader <= ADDR_LATCH;
			end case;
		end if;
	end process;
	
	driver_dual: dualport
	generic map(
      PIPE_EN         => false,  -- enable pipelined read operations
      PORT_TIME_SLOTS => "1111111111111110",
      DATA_WIDTH      => 16,  -- host & SDRAM data width
      HADDR_WIDTH     => 25  -- host-side address width
      )
    port map(
      clk             => CLOCK_100M,   -- master clock

      -- host-side port 0 (HPS)
      rst0          => '0',    -- reset
      rd0           => '0',    -- initiate read operation
      wr0           => SW_RAM_WR,    -- initiate write operation
      earlyOpBegun0 => open,    -- read/write op has begun (async)
      opBegun0      => open,   -- read/write op has begun (clocked)
      rdPending0    => open,    -- true if read operation(s) are still in the pipeline
      done0         => SW_RAM_WR_DONE,    -- read or write operation is done
      rdDone0       => open,    -- read operation is done and data is available
      hAddr0        => SW_RAM_ADDR,  -- address from host to SDRAM
      hDIn0         => SW_RAM_DATA,  -- data from host to SDRAM
      hDOut0        => open,  -- data from SDRAM to host
      status0       => open,  -- diagnostic status of the SDRAM controller FSM         

      -- host-side port 1 (FPGA)
      rst1          => '0',
      rd1           => rd_vga,
      wr1           => '0',
      earlyOpBegun1 => open,
      opBegun1      => open,
      rdPending1    => open,
      done1         => open,
      rdDone1       => rdDone_vga,
      hAddr1        => hAddr_vga,
      hDIn1         => "0000000000000000",
      hDOut1        => hDOut_vga,
      status1       => open,

      -- SDRAM controller port
      rst          => rst_sdram,
      rd           => rd_sdram,
      wr           => wr_sdram,
      earlyOpBegun => earlyOpBegun_sdram,
      opBegun      => opBegun_sdram,
      rdPending    => rdPending_sdram,
      done         => done_sdram,
      rdDone       => rdDone_sdram,
      hAddr        => hAddr_sdram,
      hDIn         => hDIn_sdram,
      hDOut        => hDOut_sdram,
      status       => status_sdram
      );
	
	SDRAM1: XSASDRAMCntl
    generic map(
      FREQ                 => 100_000,  -- operating frequency in KHz
      CLK_DIV              => 1.0,  -- divisor for FREQ (can only be 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 8.0 or 16.0)
      PIPE_EN              => false,  -- if true, enable pipelined read operations
      MAX_NOP              => 10000,  -- number of NOPs before entering self-refresh
      MULTIPLE_ACTIVE_ROWS => false,  -- if true, allow an active row in each bank
      DATA_WIDTH           => 16,  -- host & SDRAM data width
      NROWS                => 8192,  -- number of rows in SDRAM array
      NCOLS                => 1024,  -- number of columns in SDRAM array
      HADDR_WIDTH          => 25,  -- host-side address width
      SADDR_WIDTH          => 13  -- SDRAM-side address width
      )
    port map(
      -- host side
      clk                  => CLOCK_100M,--: in  std_logic;  -- master clock 
      bufclk               => open,--: out std_logic;  -- buffered master clock
      clk1x                => open,--: out std_logic;  -- host clock sync'ed to master clock (and divided if CLK_DIV>1)
      clk2x                => open,--: out std_logic;  -- double-speed host clock
      lock                 => open,--: out std_logic;  -- true when host clock is locked to master clock
      rst                  => rst_sdram,--: in  std_logic;  -- reset
      rd                   => rd_sdram,--: in  std_logic;  -- initiate read operation
      wr                   => wr_sdram,--: in  std_logic;  -- initiate write operation
      earlyOpBegun         => earlyOpBegun_sdram,--: out std_logic;  -- read/write/self-refresh op begun     (async)
      opBegun              => opBegun_sdram,--: out std_logic;  -- read/write/self-refresh op begun (clocked)
      rdPending            => rdPending_sdram,--: out std_logic;  -- read operation(s) are still in the pipeline
      done                 => done_sdram,--: out std_logic;  -- read or write operation is done
      rdDone               => rdDone_sdram,--: out std_logic;  -- read done and data is available
      hAddr                => hAddr_sdram,--: in  std_logic_vector(HADDR_WIDTH-1 downto 0);  -- address from host
      hDIn                 => hDIn_sdram,--: in  std_logic_vector(DATA_WIDTH-1 downto 0);  -- data from host
      hDOut                => HDOut_sdram,--: out std_logic_vector(DATA_WIDTH-1 downto 0);  -- data to host
      status               => status_sdram,--: out std_logic_vector(3 downto 0);  -- diagnostic status of the FSM         

      -- SDRAM side
      sclkfb => CLOCK_100M,--: in    std_logic;         -- clock from SDRAM after PCB delays
      sclk   => DRAM_CLK,--: out   std_logic;         -- SDRAM clock sync'ed to master clock
      cke    => DRAM_CKE,--: out   std_logic;         -- clock-enable to SDRAM
      cs_n   => DRAM_CS_N,--: out   std_logic;         -- chip-select to SDRAM
      ras_n  => DRAM_RAS_N,--: out   std_logic;         -- SDRAM row address strobe
      cas_n  => DRAM_CAS_N,--: out   std_logic;         -- SDRAM column address strobe
      we_n   => DRAM_WE_N,--: out   std_logic;         -- SDRAM write enable
      ba     => DRAM_BA,--: out   std_logic_vector(1 downto 0);  -- SDRAM bank address bits
      sAddr  => DRAM_ADDR,--: out   std_logic_vector(SADDR_WIDTH-1 downto 0);  -- SDRAM row/column address
      sData  => DRAM_DQ,--: inout std_logic_vector(DATA_WIDTH-1 downto 0);  -- SDRAM in/out databus
      dqmh   => DRAM_UDQM,--: out   std_logic;         -- high databits I/O mask
      dqml   => DRAM_LDQM--: out   std_logic          -- low databits I/O mask
      );
	
u0 : component hps_fpga
        port map (
            clk_clk                         => CLOCK_50,                         --                     clk.clk
            reset_reset_n                   => '1',                   --                   reset.reset_n
            memory_mem_a                    => HPS_DDR3_ADDR,                    --                  memory.mem_a
            memory_mem_ba                   => HPS_DDR3_BA,                   --                        .mem_ba
            memory_mem_ck                   => HPS_DDR3_CK_P,                   --                        .mem_ck
            memory_mem_ck_n                 => HPS_DDR3_CK_N,                 --                        .mem_ck_n
            memory_mem_cke                  => HPS_DDR3_CKE,                  --                        .mem_cke
            memory_mem_cs_n                 => HPS_DDR3_CS_N,                 --                        .mem_cs_n
            memory_mem_ras_n                => HPS_DDR3_RAS_N,                --                        .mem_ras_n
            memory_mem_cas_n                => HPS_DDR3_CAS_N,                --                        .mem_cas_n
            memory_mem_we_n                 => HPS_DDR3_WE_N,                 --                        .mem_we_n
            memory_mem_reset_n              => HPS_DDR3_RESET_N,              --                        .mem_reset_n
            memory_mem_dq                   => HPS_DDR3_DQ,                   --                        .mem_dq
            memory_mem_dqs                  => HPS_DDR3_DQS_P,                  --                        .mem_dqs
            memory_mem_dqs_n                => HPS_DDR3_DQS_N,                --                        .mem_dqs_n
            memory_mem_odt                  => HPS_DDR3_ODT,                  --                        .mem_odt
            memory_mem_dm                   => HPS_DDR3_DM,                   --                        .mem_dm
            memory_oct_rzqin                => HPS_DDR3_RZQ,                --                        .oct_rzqin
            hps_0_h2f_reset_reset_n         => HPS_H2F_RST,         --         hps_0_h2f_reset.reset_n
            led_external_connection_export  => LEDR,  -- led_external_connection.export
            sw_external_connection_export   => SW,   --  sw_external_connection.export
            hps_io_hps_io_emac1_inst_TX_CLK => HPS_ENET_GTX_CLK, --                  hps_io.hps_io_emac1_inst_TX_CLK
            hps_io_hps_io_emac1_inst_TXD0   => HPS_ENET_TX_DATA(0),   --                        .hps_io_emac1_inst_TXD0
            hps_io_hps_io_emac1_inst_TXD1   => HPS_ENET_TX_DATA(1),   --                        .hps_io_emac1_inst_TXD1
            hps_io_hps_io_emac1_inst_TXD2   => HPS_ENET_TX_DATA(2),   --                        .hps_io_emac1_inst_TXD2
            hps_io_hps_io_emac1_inst_TXD3   => HPS_ENET_TX_DATA(3),   --                        .hps_io_emac1_inst_TXD3
            hps_io_hps_io_emac1_inst_RXD0   => HPS_ENET_RX_DATA(0),   --                        .hps_io_emac1_inst_RXD0
            hps_io_hps_io_emac1_inst_MDIO   => HPS_ENET_MDIO,   --                        .hps_io_emac1_inst_MDIO
            hps_io_hps_io_emac1_inst_MDC    => HPS_ENET_MDC,    --                        .hps_io_emac1_inst_MDC
            hps_io_hps_io_emac1_inst_RX_CTL => HPS_ENET_RX_DV, --                        .hps_io_emac1_inst_RX_CTL
            hps_io_hps_io_emac1_inst_TX_CTL => HPS_ENET_TX_EN, --                        .hps_io_emac1_inst_TX_CTL
            hps_io_hps_io_emac1_inst_RX_CLK => HPS_ENET_RX_CLK, --                        .hps_io_emac1_inst_RX_CLK
            hps_io_hps_io_emac1_inst_RXD1   => HPS_ENET_RX_DATA(1),   --                        .hps_io_emac1_inst_RXD1
            hps_io_hps_io_emac1_inst_RXD2   => HPS_ENET_RX_DATA(2),   --                        .hps_io_emac1_inst_RXD2
            hps_io_hps_io_emac1_inst_RXD3   => HPS_ENET_RX_DATA(3),   --                        .hps_io_emac1_inst_RXD3
            hps_io_hps_io_sdio_inst_CMD     => HPS_SD_CMD,     --                        .hps_io_sdio_inst_CMD
            hps_io_hps_io_sdio_inst_D0      => HPS_SD_DATA(0),      --                        .hps_io_sdio_inst_D0
            hps_io_hps_io_sdio_inst_D1      => HPS_SD_DATA(1),      --                        .hps_io_sdio_inst_D1
            hps_io_hps_io_sdio_inst_CLK     => HPS_SD_CLK,     --                        .hps_io_sdio_inst_CLK
            hps_io_hps_io_sdio_inst_D2      => HPS_SD_DATA(2),      --                        .hps_io_sdio_inst_D2
            hps_io_hps_io_sdio_inst_D3      => HPS_SD_DATA(3),      --                        .hps_io_sdio_inst_D3
            hps_io_hps_io_usb1_inst_D0      => HPS_USB_DATA(0),      --                        .hps_io_usb1_inst_D0
            hps_io_hps_io_usb1_inst_D1      => HPS_USB_DATA(1),      --                        .hps_io_usb1_inst_D1
            hps_io_hps_io_usb1_inst_D2      => HPS_USB_DATA(2),      --                        .hps_io_usb1_inst_D2
            hps_io_hps_io_usb1_inst_D3      => HPS_USB_DATA(3),      --                        .hps_io_usb1_inst_D3
            hps_io_hps_io_usb1_inst_D4      => HPS_USB_DATA(4),      --                        .hps_io_usb1_inst_D4
            hps_io_hps_io_usb1_inst_D5      => HPS_USB_DATA(5),      --                        .hps_io_usb1_inst_D5
            hps_io_hps_io_usb1_inst_D6      => HPS_USB_DATA(6),      --                        .hps_io_usb1_inst_D6
            hps_io_hps_io_usb1_inst_D7      => HPS_USB_DATA(7),      --                        .hps_io_usb1_inst_D7
            hps_io_hps_io_usb1_inst_CLK     => HPS_USB_CLKOUT,     --                        .hps_io_usb1_inst_CLK
            hps_io_hps_io_usb1_inst_STP     => HPS_USB_STP,     --                        .hps_io_usb1_inst_STP
            hps_io_hps_io_usb1_inst_DIR     => HPS_USB_DIR,     --                        .hps_io_usb1_inst_DIR
            hps_io_hps_io_usb1_inst_NXT     => HPS_USB_NXT,     --                        .hps_io_usb1_inst_NXT
            hps_io_hps_io_uart0_inst_RX     => HPS_UART_RX,     --                        .hps_io_uart0_inst_RX
            hps_io_hps_io_uart0_inst_TX     => HPS_UART_TX,      --                        .hps_io_uart0_inst_TX
				ram_addr_external_connection_export    => SW_RAM_ADDR,    --    ram_addr_external_connection.export
            ram_data_wr_external_connection_export(15 downto 0) => SW_RAM_DATA, -- ram_data_wr_external_connection.export
				ram_data_wr_external_connection_export(16) => SW_RAM_WR, -- ram_data_wr_external_connection.export
            ram_wr_done_external_connection_export => SW_RAM_WR_DONE 
        );

END MAIN;