#include <stdio.h>
#include <unistd.h>
#include <fcntl.h> 
#include <sys/mman.h> 
#include "hwlib.h" 
#include "socal/socal.h"
#include "socal/hps.h" 
#include "socal/alt_gpio.h"
#include "hps_0.h"
#include "frame_reader.h"


#define REG_BASE 0xFF200000
#define REG_SPAN 0x00200000

void* virtual_base;
void* video_addr;
int fd;

int i,j;

uint32_t video_ram[1024][768];

int main(){
	fd = open("/dev/mem", (O_RDWR|O_SYNC));
	virtual_base = mmap(NULL, REG_SPAN, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, REG_BASE);
	video_addr = virtual_base + VIDEO_READER_BASE;
	
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_BASE_ADDR) = (uint32_t)video_ram;
	usleep(100);
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_WORDS) = 1024*768/4;
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_SINGLE_CYCLE_CPATERNS) = 1024*768;//????
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_WIDTH) = 1024;
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_HEIGHT) = 768;
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_0_INTERLACED) = 3;
	
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_FRAME_SELECT) = ALTERA_FRAME_READER_FRAME_SELECT_0;
	*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_CONTROL) = ALTERA_FRAME_READER_CONTROL_GO;
	
	for(i=0;i<1024;i++){
		for(j=0;j<768;j++){
			video_ram[i][j]=i+j;
		}
	}
	
	while(1){
		//*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_CONTROL) = ALTERA_FRAME_READER_CONTROL_GO;
		//*((uint32_t *)(video_addr)+ALTERA_FRAME_READER_CONTROL) = 0;
	}
	return 0;
}
