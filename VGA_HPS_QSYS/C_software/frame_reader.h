#ifndef _ALTERA_FRAME_READER_H
#define _ALTERA_FRAME_READER_H

//#define ALTERA_FRAME_READER_ 0x
#define ALTERA_FRAME_READER_CONTROL 0x0
	#define ALTERA_FRAME_READER_CONTROL_GO 0x1
	#define ALTERA_FRAME_READER_CONTROL_INT_EN 0x2

#define ALTERA_FRAME_READER_STATUS 0x1
	#define ALTERA_FRAME_READER_STATUS_STATUS 0x1
	
#define ALTERA_FRAME_READER_INTERRUPT 0x2
	#define ALTERA_FRAME_READER_INTERRUPT_RESET 0x1

#define ALTERA_FRAME_READER_FRAME_SELECT 0x3
	#define ALTERA_FRAME_READER_FRAME_SELECT_0 0x0
	#define ALTERA_FRAME_READER_FRAME_SELECT_1 0x1
	
#define ALTERA_FRAME_READER_FRAME_0_BASE_ADDR 0x4
#define ALTERA_FRAME_READER_FRAME_0_WORDS 0x5
#define ALTERA_FRAME_READER_FRAME_0_SINGLE_CYCLE_CPATERNS 0x6
#define ALTERA_FRAME_READER_FRAME_0_RESERVED 0x7
#define ALTERA_FRAME_READER_FRAME_0_WIDTH 0x8
#define ALTERA_FRAME_READER_FRAME_0_HEIGHT 0x9
#define ALTERA_FRAME_READER_FRAME_0_INTERLACED 0xA

#define ALTERA_FRAME_READER_FRAME_1_BASE_ADDR 0xB
#define ALTERA_FRAME_READER_FRAME_1_WORDS 0xC
#define ALTERA_FRAME_READER_FRAME_1_SINGLE_CYCLE_CPATERNS 0xD
#define ALTERA_FRAME_READER_FRAME_1_RESERVED 0xE
#define ALTERA_FRAME_READER_FRAME_1_WIDTH 0xF
#define ALTERA_FRAME_READER_FRAME_1_HEIGHT 0x10
#define ALTERA_FRAME_READER_FRAME_1_INTERLACED 0x11

#endif /* _ALTERA_FRAME_READER_H */
