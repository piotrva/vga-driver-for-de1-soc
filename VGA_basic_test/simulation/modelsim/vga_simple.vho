-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "02/04/2015 18:07:54"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	vga_simple IS
    PORT (
	VGA_R : OUT std_logic_vector(7 DOWNTO 0);
	VGA_G : OUT std_logic_vector(7 DOWNTO 0);
	VGA_B : OUT std_logic_vector(7 DOWNTO 0);
	VGA_CLK : OUT std_logic;
	VGA_BLANK_N : OUT std_logic;
	VGA_HS : BUFFER std_logic;
	VGA_VS : BUFFER std_logic;
	VGA_SYNC_N : OUT std_logic;
	CLOCK_50 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0)
	);
END vga_simple;

-- Design Ports Information
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_BLANK_N	=>  Location: PIN_F10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_SYNC_N	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AD11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AC9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_AD10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AE12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF vga_simple IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_VGA_R : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL ww_VGA_BLANK_N : std_logic;
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_SYNC_N : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \VGA1|Add0~21_sumout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \VGA1|Add1~117_sumout\ : std_logic;
SIGNAL \VGA1|LessThan4~6_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~5_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~7_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~3_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~1_combout\ : std_logic;
SIGNAL \VGA1|px_counter[18]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|px_counter[19]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|px_counter[21]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|LessThan4~2_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~4_combout\ : std_logic;
SIGNAL \VGA1|px_counter[22]~0_combout\ : std_logic;
SIGNAL \VGA1|Add1~118\ : std_logic;
SIGNAL \VGA1|Add1~121_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[1]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~122\ : std_logic;
SIGNAL \VGA1|Add1~125_sumout\ : std_logic;
SIGNAL \VGA1|Add1~126\ : std_logic;
SIGNAL \VGA1|Add1~109_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[3]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~110\ : std_logic;
SIGNAL \VGA1|Add1~113_sumout\ : std_logic;
SIGNAL \VGA1|Add1~114\ : std_logic;
SIGNAL \VGA1|Add1~9_sumout\ : std_logic;
SIGNAL \VGA1|Add1~10\ : std_logic;
SIGNAL \VGA1|Add1~13_sumout\ : std_logic;
SIGNAL \VGA1|Add1~14\ : std_logic;
SIGNAL \VGA1|Add1~17_sumout\ : std_logic;
SIGNAL \VGA1|Add1~18\ : std_logic;
SIGNAL \VGA1|Add1~1_sumout\ : std_logic;
SIGNAL \VGA1|Add1~2\ : std_logic;
SIGNAL \VGA1|Add1~5_sumout\ : std_logic;
SIGNAL \VGA1|Add1~6\ : std_logic;
SIGNAL \VGA1|Add1~21_sumout\ : std_logic;
SIGNAL \VGA1|Add1~22\ : std_logic;
SIGNAL \VGA1|Add1~97_sumout\ : std_logic;
SIGNAL \VGA1|Add1~98\ : std_logic;
SIGNAL \VGA1|Add1~101_sumout\ : std_logic;
SIGNAL \VGA1|Add1~102\ : std_logic;
SIGNAL \VGA1|Add1~93_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[13]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~94\ : std_logic;
SIGNAL \VGA1|Add1~89_sumout\ : std_logic;
SIGNAL \VGA1|Add1~90\ : std_logic;
SIGNAL \VGA1|Add1~85_sumout\ : std_logic;
SIGNAL \VGA1|Add1~86\ : std_logic;
SIGNAL \VGA1|Add1~81_sumout\ : std_logic;
SIGNAL \VGA1|Add1~82\ : std_logic;
SIGNAL \VGA1|Add1~77_sumout\ : std_logic;
SIGNAL \VGA1|Add1~78\ : std_logic;
SIGNAL \VGA1|Add1~73_sumout\ : std_logic;
SIGNAL \VGA1|Add1~74\ : std_logic;
SIGNAL \VGA1|Add1~69_sumout\ : std_logic;
SIGNAL \VGA1|Add1~70\ : std_logic;
SIGNAL \VGA1|Add1~65_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[20]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~66\ : std_logic;
SIGNAL \VGA1|Add1~61_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[21]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~62\ : std_logic;
SIGNAL \VGA1|Add1~57_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[22]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~58\ : std_logic;
SIGNAL \VGA1|Add1~29_sumout\ : std_logic;
SIGNAL \VGA1|Add1~30\ : std_logic;
SIGNAL \VGA1|Add1~25_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[24]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~26\ : std_logic;
SIGNAL \VGA1|Add1~53_sumout\ : std_logic;
SIGNAL \VGA1|Add1~54\ : std_logic;
SIGNAL \VGA1|Add1~49_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[26]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~50\ : std_logic;
SIGNAL \VGA1|Add1~45_sumout\ : std_logic;
SIGNAL \VGA1|Add1~46\ : std_logic;
SIGNAL \VGA1|Add1~41_sumout\ : std_logic;
SIGNAL \VGA1|px_counter[28]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add1~42\ : std_logic;
SIGNAL \VGA1|Add1~37_sumout\ : std_logic;
SIGNAL \VGA1|Add1~38\ : std_logic;
SIGNAL \VGA1|Add1~33_sumout\ : std_logic;
SIGNAL \VGA1|Add1~34\ : std_logic;
SIGNAL \VGA1|Add1~105_sumout\ : std_logic;
SIGNAL \VGA1|LessThan0~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan0~1_combout\ : std_logic;
SIGNAL \VGA1|LessThan0~2_combout\ : std_logic;
SIGNAL \VGA1|Add2~125_sumout\ : std_logic;
SIGNAL \VGA1|LessThan1~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan5~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan5~1_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~2_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~3_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~1_combout\ : std_logic;
SIGNAL \VGA1|line_counter[15]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~1_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~0_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~4_combout\ : std_logic;
SIGNAL \VGA1|LessThan1~1_combout\ : std_logic;
SIGNAL \VGA1|LessThan6~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan7~1_combout\ : std_logic;
SIGNAL \VGA1|LessThan7~0_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~6_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~7_combout\ : std_logic;
SIGNAL \VGA1|Add2~126\ : std_logic;
SIGNAL \VGA1|Add2~121_sumout\ : std_logic;
SIGNAL \VGA1|Add2~122\ : std_logic;
SIGNAL \VGA1|Add2~117_sumout\ : std_logic;
SIGNAL \VGA1|Add2~118\ : std_logic;
SIGNAL \VGA1|Add2~17_sumout\ : std_logic;
SIGNAL \VGA1|Add2~18\ : std_logic;
SIGNAL \VGA1|Add2~13_sumout\ : std_logic;
SIGNAL \VGA1|Add2~14\ : std_logic;
SIGNAL \VGA1|Add2~9_sumout\ : std_logic;
SIGNAL \VGA1|Add2~10\ : std_logic;
SIGNAL \VGA1|Add2~5_sumout\ : std_logic;
SIGNAL \VGA1|Add2~6\ : std_logic;
SIGNAL \VGA1|Add2~25_sumout\ : std_logic;
SIGNAL \VGA1|Add2~26\ : std_logic;
SIGNAL \VGA1|Add2~21_sumout\ : std_logic;
SIGNAL \VGA1|Add2~22\ : std_logic;
SIGNAL \VGA1|Add2~1_sumout\ : std_logic;
SIGNAL \VGA1|Add2~2\ : std_logic;
SIGNAL \VGA1|Add2~73_sumout\ : std_logic;
SIGNAL \VGA1|Add2~74\ : std_logic;
SIGNAL \VGA1|Add2~45_sumout\ : std_logic;
SIGNAL \VGA1|Add2~46\ : std_logic;
SIGNAL \VGA1|Add2~41_sumout\ : std_logic;
SIGNAL \VGA1|Add2~42\ : std_logic;
SIGNAL \VGA1|Add2~37_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[13]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~38\ : std_logic;
SIGNAL \VGA1|Add2~69_sumout\ : std_logic;
SIGNAL \VGA1|Add2~70\ : std_logic;
SIGNAL \VGA1|Add2~65_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[15]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~66\ : std_logic;
SIGNAL \VGA1|Add2~61_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[16]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~62\ : std_logic;
SIGNAL \VGA1|Add2~57_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[17]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~58\ : std_logic;
SIGNAL \VGA1|Add2~53_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[18]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~54\ : std_logic;
SIGNAL \VGA1|Add2~49_sumout\ : std_logic;
SIGNAL \VGA1|Add2~50\ : std_logic;
SIGNAL \VGA1|Add2~33_sumout\ : std_logic;
SIGNAL \VGA1|Add2~34\ : std_logic;
SIGNAL \VGA1|Add2~109_sumout\ : std_logic;
SIGNAL \VGA1|Add2~110\ : std_logic;
SIGNAL \VGA1|Add2~105_sumout\ : std_logic;
SIGNAL \VGA1|Add2~106\ : std_logic;
SIGNAL \VGA1|Add2~101_sumout\ : std_logic;
SIGNAL \VGA1|Add2~102\ : std_logic;
SIGNAL \VGA1|Add2~97_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[24]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~98\ : std_logic;
SIGNAL \VGA1|Add2~93_sumout\ : std_logic;
SIGNAL \VGA1|line_counter[25]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add2~94\ : std_logic;
SIGNAL \VGA1|Add2~29_sumout\ : std_logic;
SIGNAL \VGA1|Add2~30\ : std_logic;
SIGNAL \VGA1|Add2~89_sumout\ : std_logic;
SIGNAL \VGA1|Add2~90\ : std_logic;
SIGNAL \VGA1|Add2~85_sumout\ : std_logic;
SIGNAL \VGA1|Add2~86\ : std_logic;
SIGNAL \VGA1|Add2~81_sumout\ : std_logic;
SIGNAL \VGA1|Add2~82\ : std_logic;
SIGNAL \VGA1|Add2~77_sumout\ : std_logic;
SIGNAL \VGA1|Add2~78\ : std_logic;
SIGNAL \VGA1|Add2~113_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~5_combout\ : std_logic;
SIGNAL \VGA1|LessThan4~8_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~8_combout\ : std_logic;
SIGNAL \VGA1|Add0~22\ : std_logic;
SIGNAL \VGA1|Add0~29_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[1]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add0~30\ : std_logic;
SIGNAL \VGA1|Add0~33_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[2]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|Add0~34\ : std_logic;
SIGNAL \VGA1|Add0~37_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[3]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add0~38\ : std_logic;
SIGNAL \VGA1|Add0~13_sumout\ : std_logic;
SIGNAL \VGA1|Add0~14\ : std_logic;
SIGNAL \VGA1|Add0~17_sumout\ : std_logic;
SIGNAL \VGA1|Add0~18\ : std_logic;
SIGNAL \VGA1|Add0~41_sumout\ : std_logic;
SIGNAL \VGA1|Add0~42\ : std_logic;
SIGNAL \VGA1|Add0~45_sumout\ : std_logic;
SIGNAL \VGA1|Add0~46\ : std_logic;
SIGNAL \VGA1|Add0~25_sumout\ : std_logic;
SIGNAL \VGA1|Add0~26\ : std_logic;
SIGNAL \VGA1|Add0~53_sumout\ : std_logic;
SIGNAL \VGA1|Add0~54\ : std_logic;
SIGNAL \VGA1|Add0~57_sumout\ : std_logic;
SIGNAL \VGA1|Add0~58\ : std_logic;
SIGNAL \VGA1|Add0~49_sumout\ : std_logic;
SIGNAL \VGA1|Add0~50\ : std_logic;
SIGNAL \VGA1|Add0~69_sumout\ : std_logic;
SIGNAL \VGA1|Add0~70\ : std_logic;
SIGNAL \VGA1|Add0~65_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[13]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add0~66\ : std_logic;
SIGNAL \VGA1|Add0~61_sumout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \VGA1|Add0~62\ : std_logic;
SIGNAL \VGA1|Add0~9_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[15]~feeder_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \VGA1|Add0~10\ : std_logic;
SIGNAL \VGA1|Add0~5_sumout\ : std_logic;
SIGNAL \VGA1|Add0~6\ : std_logic;
SIGNAL \VGA1|Add0~1_sumout\ : std_logic;
SIGNAL \VGA1|Add0~2\ : std_logic;
SIGNAL \VGA1|Add0~81_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[18]~feeder_combout\ : std_logic;
SIGNAL \VGA1|Add0~82\ : std_logic;
SIGNAL \VGA1|Add0~77_sumout\ : std_logic;
SIGNAL \VGA1|Add0~78\ : std_logic;
SIGNAL \VGA1|Add0~73_sumout\ : std_logic;
SIGNAL \VGA1|PX_ADDR[20]~feeder_combout\ : std_logic;
SIGNAL \LessThan0~4_combout\ : std_logic;
SIGNAL \LessThan0~5_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[0]~1_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[0]~0_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[1]~2_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[2]~3_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[3]~4_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[4]~5_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[5]~6_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[6]~7_combout\ : std_logic;
SIGNAL \VGA1|VGA_R[7]~8_combout\ : std_logic;
SIGNAL \VGA1|VGA_G[3]~feeder_combout\ : std_logic;
SIGNAL \VGA1|VGA_G[5]~feeder_combout\ : std_logic;
SIGNAL \VGA1|LessThan3~0_combout\ : std_logic;
SIGNAL \VGA1|LessThan3~1_combout\ : std_logic;
SIGNAL \VGA1|LessThan2~0_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~0_combout\ : std_logic;
SIGNAL \VGA1|VGA_BLANK_N~0_combout\ : std_logic;
SIGNAL \VGA1|VGA_BLANK_N~q\ : std_logic;
SIGNAL \VGA1|LessThan3~2_combout\ : std_logic;
SIGNAL \VGA1|VGA_HS~0_combout\ : std_logic;
SIGNAL \VGA1|VGA_HS~q\ : std_logic;
SIGNAL \VGA1|LessThan6~1_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~2_combout\ : std_logic;
SIGNAL \VGA1|LessThan7~2_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~3_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~4_combout\ : std_logic;
SIGNAL \VGA1|VGA_VS~q\ : std_logic;
SIGNAL \VGA1|VGA_SYNC_N~0_combout\ : std_logic;
SIGNAL \VGA1|PX_ADDR\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \VGA1|line_counter\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \VGA1|px_counter\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \VGA1|VGA_R\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|fboutclk_wire\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \VGA1|VGA_G\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \VGA1|ALT_INV_px_counter[18]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|ALT_INV_px_counter[19]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|ALT_INV_px_counter[21]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|ALT_INV_line_counter[15]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[2]~DUPLICATE_q\ : std_logic;
SIGNAL \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~7_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~6_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_VS~3_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan7~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan7~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_VS~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan5~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan5~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan6~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan6~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_VS~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan3~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~8_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~7_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~6_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~5_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~4_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_VS~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan2~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan3~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan3~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~5_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~3_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan4~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~4_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~3_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~2_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_PX_ADDR[20]~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan1~1_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_LessThan1~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~5_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_SYNC_N~0_combout\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_VS~q\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_HS~q\ : std_logic;
SIGNAL \VGA1|ALT_INV_VGA_BLANK_N~q\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~121_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~109_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~93_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~65_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~61_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~57_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~49_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~41_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~97_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~93_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~65_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~61_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~57_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~53_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add2~37_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~81_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~73_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~65_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~37_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \VGA1|ALT_INV_line_counter\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \VGA1|ALT_INV_px_counter\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \VGA1|ALT_INV_PX_ADDR\ : std_logic_vector(20 DOWNTO 0);

BEGIN

VGA_R <= ww_VGA_R;
VGA_G <= ww_VGA_G;
VGA_B <= ww_VGA_B;
VGA_CLK <= ww_VGA_CLK;
VGA_BLANK_N <= ww_VGA_BLANK_N;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_SYNC_N <= ww_VGA_SYNC_N;
ww_CLOCK_50 <= CLOCK_50;
ww_SW <= SW;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(0);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(1);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(2);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(3);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(4);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(5);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(6);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(7);

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(0);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(1);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(2);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(3);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(4);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(5);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(6);
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(7);

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\ <= (\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ & 
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ & 
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\);

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\ <= \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\(6);

\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ & 
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ & 
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ & \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\);
\VGA1|ALT_INV_px_counter[18]~DUPLICATE_q\ <= NOT \VGA1|px_counter[18]~DUPLICATE_q\;
\VGA1|ALT_INV_px_counter[19]~DUPLICATE_q\ <= NOT \VGA1|px_counter[19]~DUPLICATE_q\;
\VGA1|ALT_INV_px_counter[21]~DUPLICATE_q\ <= NOT \VGA1|px_counter[21]~DUPLICATE_q\;
\VGA1|ALT_INV_line_counter[15]~DUPLICATE_q\ <= NOT \VGA1|line_counter[15]~DUPLICATE_q\;
\VGA1|ALT_INV_PX_ADDR[2]~DUPLICATE_q\ <= NOT \VGA1|PX_ADDR[2]~DUPLICATE_q\;
\PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\ <= NOT \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\VGA1|ALT_INV_PX_ADDR[20]~7_combout\ <= NOT \VGA1|PX_ADDR[20]~7_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~6_combout\ <= NOT \VGA1|PX_ADDR[20]~6_combout\;
\VGA1|ALT_INV_VGA_VS~3_combout\ <= NOT \VGA1|VGA_VS~3_combout\;
\VGA1|ALT_INV_LessThan7~2_combout\ <= NOT \VGA1|LessThan7~2_combout\;
\VGA1|ALT_INV_LessThan7~1_combout\ <= NOT \VGA1|LessThan7~1_combout\;
\VGA1|ALT_INV_LessThan7~0_combout\ <= NOT \VGA1|LessThan7~0_combout\;
\VGA1|ALT_INV_VGA_VS~2_combout\ <= NOT \VGA1|VGA_VS~2_combout\;
\VGA1|ALT_INV_LessThan5~1_combout\ <= NOT \VGA1|LessThan5~1_combout\;
\VGA1|ALT_INV_LessThan5~0_combout\ <= NOT \VGA1|LessThan5~0_combout\;
\VGA1|ALT_INV_LessThan6~1_combout\ <= NOT \VGA1|LessThan6~1_combout\;
\VGA1|ALT_INV_LessThan6~0_combout\ <= NOT \VGA1|LessThan6~0_combout\;
\VGA1|ALT_INV_VGA_VS~1_combout\ <= NOT \VGA1|VGA_VS~1_combout\;
\VGA1|ALT_INV_LessThan3~2_combout\ <= NOT \VGA1|LessThan3~2_combout\;
\VGA1|ALT_INV_LessThan4~8_combout\ <= NOT \VGA1|LessThan4~8_combout\;
\VGA1|ALT_INV_LessThan4~7_combout\ <= NOT \VGA1|LessThan4~7_combout\;
\VGA1|ALT_INV_LessThan4~6_combout\ <= NOT \VGA1|LessThan4~6_combout\;
\VGA1|ALT_INV_LessThan4~5_combout\ <= NOT \VGA1|LessThan4~5_combout\;
\VGA1|ALT_INV_LessThan4~4_combout\ <= NOT \VGA1|LessThan4~4_combout\;
\VGA1|ALT_INV_VGA_VS~0_combout\ <= NOT \VGA1|VGA_VS~0_combout\;
\VGA1|ALT_INV_LessThan2~0_combout\ <= NOT \VGA1|LessThan2~0_combout\;
\VGA1|ALT_INV_LessThan3~1_combout\ <= NOT \VGA1|LessThan3~1_combout\;
\VGA1|ALT_INV_LessThan3~0_combout\ <= NOT \VGA1|LessThan3~0_combout\;
\VGA1|ALT_INV_LessThan0~2_combout\ <= NOT \VGA1|LessThan0~2_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~5_combout\ <= NOT \VGA1|PX_ADDR[20]~5_combout\;
\VGA1|ALT_INV_LessThan0~1_combout\ <= NOT \VGA1|LessThan0~1_combout\;
\VGA1|ALT_INV_LessThan4~3_combout\ <= NOT \VGA1|LessThan4~3_combout\;
\VGA1|ALT_INV_LessThan4~2_combout\ <= NOT \VGA1|LessThan4~2_combout\;
\VGA1|ALT_INV_LessThan4~1_combout\ <= NOT \VGA1|LessThan4~1_combout\;
\VGA1|ALT_INV_LessThan4~0_combout\ <= NOT \VGA1|LessThan4~0_combout\;
\VGA1|ALT_INV_LessThan0~0_combout\ <= NOT \VGA1|LessThan0~0_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~4_combout\ <= NOT \VGA1|PX_ADDR[20]~4_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~3_combout\ <= NOT \VGA1|PX_ADDR[20]~3_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~2_combout\ <= NOT \VGA1|PX_ADDR[20]~2_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~1_combout\ <= NOT \VGA1|PX_ADDR[20]~1_combout\;
\VGA1|ALT_INV_PX_ADDR[20]~0_combout\ <= NOT \VGA1|PX_ADDR[20]~0_combout\;
\VGA1|ALT_INV_LessThan1~1_combout\ <= NOT \VGA1|LessThan1~1_combout\;
\VGA1|ALT_INV_LessThan1~0_combout\ <= NOT \VGA1|LessThan1~0_combout\;
\ALT_INV_LessThan0~5_combout\ <= NOT \LessThan0~5_combout\;
\ALT_INV_LessThan0~4_combout\ <= NOT \LessThan0~4_combout\;
\ALT_INV_LessThan0~3_combout\ <= NOT \LessThan0~3_combout\;
\ALT_INV_LessThan0~2_combout\ <= NOT \LessThan0~2_combout\;
\ALT_INV_LessThan0~1_combout\ <= NOT \LessThan0~1_combout\;
\ALT_INV_LessThan0~0_combout\ <= NOT \LessThan0~0_combout\;
\VGA1|ALT_INV_VGA_SYNC_N~0_combout\ <= NOT \VGA1|VGA_SYNC_N~0_combout\;
\VGA1|ALT_INV_VGA_VS~q\ <= NOT \VGA1|VGA_VS~q\;
\VGA1|ALT_INV_VGA_HS~q\ <= NOT \VGA1|VGA_HS~q\;
\VGA1|ALT_INV_VGA_BLANK_N~q\ <= NOT \VGA1|VGA_BLANK_N~q\;
\VGA1|ALT_INV_Add1~121_sumout\ <= NOT \VGA1|Add1~121_sumout\;
\VGA1|ALT_INV_Add1~109_sumout\ <= NOT \VGA1|Add1~109_sumout\;
\VGA1|ALT_INV_Add1~93_sumout\ <= NOT \VGA1|Add1~93_sumout\;
\VGA1|ALT_INV_Add1~65_sumout\ <= NOT \VGA1|Add1~65_sumout\;
\VGA1|ALT_INV_Add1~61_sumout\ <= NOT \VGA1|Add1~61_sumout\;
\VGA1|ALT_INV_Add1~57_sumout\ <= NOT \VGA1|Add1~57_sumout\;
\VGA1|ALT_INV_Add1~49_sumout\ <= NOT \VGA1|Add1~49_sumout\;
\VGA1|ALT_INV_Add1~41_sumout\ <= NOT \VGA1|Add1~41_sumout\;
\VGA1|ALT_INV_Add1~25_sumout\ <= NOT \VGA1|Add1~25_sumout\;
\VGA1|ALT_INV_Add2~97_sumout\ <= NOT \VGA1|Add2~97_sumout\;
\VGA1|ALT_INV_Add2~93_sumout\ <= NOT \VGA1|Add2~93_sumout\;
\VGA1|ALT_INV_Add2~65_sumout\ <= NOT \VGA1|Add2~65_sumout\;
\VGA1|ALT_INV_Add2~61_sumout\ <= NOT \VGA1|Add2~61_sumout\;
\VGA1|ALT_INV_Add2~57_sumout\ <= NOT \VGA1|Add2~57_sumout\;
\VGA1|ALT_INV_Add2~53_sumout\ <= NOT \VGA1|Add2~53_sumout\;
\VGA1|ALT_INV_Add2~37_sumout\ <= NOT \VGA1|Add2~37_sumout\;
\VGA1|ALT_INV_Add0~81_sumout\ <= NOT \VGA1|Add0~81_sumout\;
\VGA1|ALT_INV_Add0~73_sumout\ <= NOT \VGA1|Add0~73_sumout\;
\VGA1|ALT_INV_Add0~65_sumout\ <= NOT \VGA1|Add0~65_sumout\;
\VGA1|ALT_INV_Add0~37_sumout\ <= NOT \VGA1|Add0~37_sumout\;
\VGA1|ALT_INV_Add0~29_sumout\ <= NOT \VGA1|Add0~29_sumout\;
\VGA1|ALT_INV_Add0~9_sumout\ <= NOT \VGA1|Add0~9_sumout\;
\VGA1|ALT_INV_line_counter\(0) <= NOT \VGA1|line_counter\(0);
\VGA1|ALT_INV_line_counter\(1) <= NOT \VGA1|line_counter\(1);
\VGA1|ALT_INV_line_counter\(2) <= NOT \VGA1|line_counter\(2);
\VGA1|ALT_INV_px_counter\(2) <= NOT \VGA1|px_counter\(2);
\VGA1|ALT_INV_px_counter\(1) <= NOT \VGA1|px_counter\(1);
\VGA1|ALT_INV_px_counter\(0) <= NOT \VGA1|px_counter\(0);
\VGA1|ALT_INV_px_counter\(4) <= NOT \VGA1|px_counter\(4);
\VGA1|ALT_INV_px_counter\(3) <= NOT \VGA1|px_counter\(3);
\VGA1|ALT_INV_px_counter\(31) <= NOT \VGA1|px_counter\(31);
\VGA1|ALT_INV_px_counter\(12) <= NOT \VGA1|px_counter\(12);
\VGA1|ALT_INV_px_counter\(11) <= NOT \VGA1|px_counter\(11);
\VGA1|ALT_INV_px_counter\(13) <= NOT \VGA1|px_counter\(13);
\VGA1|ALT_INV_px_counter\(14) <= NOT \VGA1|px_counter\(14);
\VGA1|ALT_INV_px_counter\(15) <= NOT \VGA1|px_counter\(15);
\VGA1|ALT_INV_px_counter\(16) <= NOT \VGA1|px_counter\(16);
\VGA1|ALT_INV_px_counter\(17) <= NOT \VGA1|px_counter\(17);
\VGA1|ALT_INV_px_counter\(18) <= NOT \VGA1|px_counter\(18);
\VGA1|ALT_INV_px_counter\(19) <= NOT \VGA1|px_counter\(19);
\VGA1|ALT_INV_px_counter\(20) <= NOT \VGA1|px_counter\(20);
\VGA1|ALT_INV_px_counter\(21) <= NOT \VGA1|px_counter\(21);
\VGA1|ALT_INV_px_counter\(22) <= NOT \VGA1|px_counter\(22);
\VGA1|ALT_INV_px_counter\(25) <= NOT \VGA1|px_counter\(25);
\VGA1|ALT_INV_px_counter\(26) <= NOT \VGA1|px_counter\(26);
\VGA1|ALT_INV_px_counter\(27) <= NOT \VGA1|px_counter\(27);
\VGA1|ALT_INV_px_counter\(28) <= NOT \VGA1|px_counter\(28);
\VGA1|ALT_INV_px_counter\(29) <= NOT \VGA1|px_counter\(29);
\VGA1|ALT_INV_px_counter\(30) <= NOT \VGA1|px_counter\(30);
\VGA1|ALT_INV_px_counter\(23) <= NOT \VGA1|px_counter\(23);
\VGA1|ALT_INV_px_counter\(24) <= NOT \VGA1|px_counter\(24);
\VGA1|ALT_INV_px_counter\(10) <= NOT \VGA1|px_counter\(10);
\VGA1|ALT_INV_px_counter\(7) <= NOT \VGA1|px_counter\(7);
\VGA1|ALT_INV_px_counter\(6) <= NOT \VGA1|px_counter\(6);
\VGA1|ALT_INV_px_counter\(5) <= NOT \VGA1|px_counter\(5);
\VGA1|ALT_INV_px_counter\(9) <= NOT \VGA1|px_counter\(9);
\VGA1|ALT_INV_px_counter\(8) <= NOT \VGA1|px_counter\(8);
\VGA1|ALT_INV_line_counter\(31) <= NOT \VGA1|line_counter\(31);
\VGA1|ALT_INV_line_counter\(21) <= NOT \VGA1|line_counter\(21);
\VGA1|ALT_INV_line_counter\(22) <= NOT \VGA1|line_counter\(22);
\VGA1|ALT_INV_line_counter\(23) <= NOT \VGA1|line_counter\(23);
\VGA1|ALT_INV_line_counter\(24) <= NOT \VGA1|line_counter\(24);
\VGA1|ALT_INV_line_counter\(25) <= NOT \VGA1|line_counter\(25);
\VGA1|ALT_INV_line_counter\(27) <= NOT \VGA1|line_counter\(27);
\VGA1|ALT_INV_line_counter\(28) <= NOT \VGA1|line_counter\(28);
\VGA1|ALT_INV_line_counter\(29) <= NOT \VGA1|line_counter\(29);
\VGA1|ALT_INV_line_counter\(30) <= NOT \VGA1|line_counter\(30);
\VGA1|ALT_INV_line_counter\(10) <= NOT \VGA1|line_counter\(10);
\VGA1|ALT_INV_line_counter\(14) <= NOT \VGA1|line_counter\(14);
\VGA1|ALT_INV_line_counter\(15) <= NOT \VGA1|line_counter\(15);
\VGA1|ALT_INV_line_counter\(16) <= NOT \VGA1|line_counter\(16);
\VGA1|ALT_INV_line_counter\(17) <= NOT \VGA1|line_counter\(17);
\VGA1|ALT_INV_line_counter\(18) <= NOT \VGA1|line_counter\(18);
\VGA1|ALT_INV_line_counter\(19) <= NOT \VGA1|line_counter\(19);
\VGA1|ALT_INV_line_counter\(11) <= NOT \VGA1|line_counter\(11);
\VGA1|ALT_INV_line_counter\(12) <= NOT \VGA1|line_counter\(12);
\VGA1|ALT_INV_line_counter\(13) <= NOT \VGA1|line_counter\(13);
\VGA1|ALT_INV_line_counter\(20) <= NOT \VGA1|line_counter\(20);
\VGA1|ALT_INV_line_counter\(26) <= NOT \VGA1|line_counter\(26);
\VGA1|ALT_INV_line_counter\(7) <= NOT \VGA1|line_counter\(7);
\VGA1|ALT_INV_line_counter\(8) <= NOT \VGA1|line_counter\(8);
\VGA1|ALT_INV_line_counter\(3) <= NOT \VGA1|line_counter\(3);
\VGA1|ALT_INV_line_counter\(4) <= NOT \VGA1|line_counter\(4);
\VGA1|ALT_INV_line_counter\(5) <= NOT \VGA1|line_counter\(5);
\VGA1|ALT_INV_line_counter\(6) <= NOT \VGA1|line_counter\(6);
\VGA1|ALT_INV_line_counter\(9) <= NOT \VGA1|line_counter\(9);
\VGA1|ALT_INV_PX_ADDR\(18) <= NOT \VGA1|PX_ADDR\(18);
\VGA1|ALT_INV_PX_ADDR\(19) <= NOT \VGA1|PX_ADDR\(19);
\VGA1|ALT_INV_PX_ADDR\(20) <= NOT \VGA1|PX_ADDR\(20);
\VGA1|ALT_INV_PX_ADDR\(12) <= NOT \VGA1|PX_ADDR\(12);
\VGA1|ALT_INV_PX_ADDR\(13) <= NOT \VGA1|PX_ADDR\(13);
\VGA1|ALT_INV_PX_ADDR\(14) <= NOT \VGA1|PX_ADDR\(14);
\VGA1|ALT_INV_PX_ADDR\(10) <= NOT \VGA1|PX_ADDR\(10);
\VGA1|ALT_INV_PX_ADDR\(9) <= NOT \VGA1|PX_ADDR\(9);
\VGA1|ALT_INV_PX_ADDR\(11) <= NOT \VGA1|PX_ADDR\(11);
\VGA1|ALT_INV_PX_ADDR\(7) <= NOT \VGA1|PX_ADDR\(7);
\VGA1|ALT_INV_PX_ADDR\(6) <= NOT \VGA1|PX_ADDR\(6);
\VGA1|ALT_INV_PX_ADDR\(3) <= NOT \VGA1|PX_ADDR\(3);
\VGA1|ALT_INV_PX_ADDR\(2) <= NOT \VGA1|PX_ADDR\(2);
\VGA1|ALT_INV_PX_ADDR\(1) <= NOT \VGA1|PX_ADDR\(1);
\VGA1|ALT_INV_PX_ADDR\(8) <= NOT \VGA1|PX_ADDR\(8);
\VGA1|ALT_INV_PX_ADDR\(0) <= NOT \VGA1|PX_ADDR\(0);
\VGA1|ALT_INV_PX_ADDR\(5) <= NOT \VGA1|PX_ADDR\(5);
\VGA1|ALT_INV_PX_ADDR\(4) <= NOT \VGA1|PX_ADDR\(4);
\VGA1|ALT_INV_PX_ADDR\(15) <= NOT \VGA1|PX_ADDR\(15);
\VGA1|ALT_INV_PX_ADDR\(16) <= NOT \VGA1|PX_ADDR\(16);
\VGA1|ALT_INV_PX_ADDR\(17) <= NOT \VGA1|PX_ADDR\(17);

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(0),
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(1),
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(2),
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(3),
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(4),
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(5),
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(6),
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_R\(7),
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(0),
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(1),
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(2),
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(3),
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(4),
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(5),
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(6),
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_G\(7),
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOOBUF_X6_Y81_N19
\VGA_BLANK_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_BLANK_N~q\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK_N);

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|VGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X28_Y81_N36
\VGA_SYNC_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA1|ALT_INV_VGA_SYNC_N~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_SYNC_N);

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "none",
	nreset_invert => "true",
	output_clock_frequency => "320.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 6000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 20,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "m_cnt",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 16,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 16,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 2,
	pll_m_cnt_prst => 4,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 3,
	pll_n_cnt_lo_div => 2,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "true",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \PLL_VGA|pll_1028_inst|altera_pll_i|fboutclk_wire\(0),
	ecnc1test => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiften => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\,
	up => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	cntnen => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \PLL_VGA|pll_1028_inst|altera_pll_i|fboutclk_wire\(0),
	tclk => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\,
	shift => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\,
	up => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	shiften => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 4,
	dprio0_cnt_lo_div => 4,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "40.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiften => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\,
	up0 => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	vco0ph => \PLL_VGA|pll_1028_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire\(0));

-- Location: CLKCTRL_G6
\PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire\(0),
	outclk => \PLL_VGA|pll_1028_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\);

-- Location: LABCELL_X27_Y35_N30
\VGA1|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~21_sumout\ = SUM(( \VGA1|PX_ADDR\(0) ) + ( VCC ) + ( !VCC ))
-- \VGA1|Add0~22\ = CARRY(( \VGA1|PX_ADDR\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \VGA1|ALT_INV_PX_ADDR\(0),
	cin => GND,
	sumout => \VGA1|Add0~21_sumout\,
	cout => \VGA1|Add0~22\);

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: MLABCELL_X28_Y31_N0
\VGA1|Add1~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~117_sumout\ = SUM(( \VGA1|px_counter\(0) ) + ( VCC ) + ( !VCC ))
-- \VGA1|Add1~118\ = CARRY(( \VGA1|px_counter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \VGA1|ALT_INV_px_counter\(0),
	cin => GND,
	sumout => \VGA1|Add1~117_sumout\,
	cout => \VGA1|Add1~118\);

-- Location: LABCELL_X29_Y31_N33
\VGA1|LessThan4~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~6_combout\ = ( !\VGA1|px_counter\(7) & ( (!\VGA1|px_counter\(9) & (!\VGA1|px_counter\(6) & !\VGA1|px_counter\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(9),
	datac => \VGA1|ALT_INV_px_counter\(6),
	datad => \VGA1|ALT_INV_px_counter\(5),
	dataf => \VGA1|ALT_INV_px_counter\(7),
	combout => \VGA1|LessThan4~6_combout\);

-- Location: LABCELL_X29_Y31_N24
\VGA1|LessThan4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~5_combout\ = ( \VGA1|px_counter\(3) & ( (\VGA1|px_counter\(0) & (\VGA1|px_counter\(1) & \VGA1|px_counter\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(0),
	datac => \VGA1|ALT_INV_px_counter\(1),
	datad => \VGA1|ALT_INV_px_counter\(2),
	dataf => \VGA1|ALT_INV_px_counter\(3),
	combout => \VGA1|LessThan4~5_combout\);

-- Location: LABCELL_X29_Y31_N30
\VGA1|LessThan4~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~7_combout\ = ( \VGA1|LessThan4~5_combout\ & ( (!\VGA1|px_counter\(4) & (\VGA1|LessThan4~6_combout\ & !\VGA1|px_counter\(8))) ) ) # ( !\VGA1|LessThan4~5_combout\ & ( (\VGA1|LessThan4~6_combout\ & !\VGA1|px_counter\(8)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(4),
	datac => \VGA1|ALT_INV_LessThan4~6_combout\,
	datad => \VGA1|ALT_INV_px_counter\(8),
	dataf => \VGA1|ALT_INV_LessThan4~5_combout\,
	combout => \VGA1|LessThan4~7_combout\);

-- Location: LABCELL_X29_Y31_N0
\VGA1|LessThan4~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~3_combout\ = ( !\VGA1|px_counter\(12) & ( !\VGA1|px_counter\(14) & ( (!\VGA1|px_counter\(16) & (!\VGA1|px_counter\(15) & (!\VGA1|px_counter\(13) & !\VGA1|px_counter\(11)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(16),
	datab => \VGA1|ALT_INV_px_counter\(15),
	datac => \VGA1|ALT_INV_px_counter\(13),
	datad => \VGA1|ALT_INV_px_counter\(11),
	datae => \VGA1|ALT_INV_px_counter\(12),
	dataf => \VGA1|ALT_INV_px_counter\(14),
	combout => \VGA1|LessThan4~3_combout\);

-- Location: MLABCELL_X28_Y30_N42
\VGA1|LessThan4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~0_combout\ = ( !\VGA1|px_counter\(23) & ( !\VGA1|px_counter\(24) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011110000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(24),
	datae => \VGA1|ALT_INV_px_counter\(23),
	combout => \VGA1|LessThan4~0_combout\);

-- Location: MLABCELL_X28_Y30_N48
\VGA1|LessThan4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~1_combout\ = ( !\VGA1|px_counter\(25) & ( !\VGA1|px_counter\(26) & ( (!\VGA1|px_counter\(30) & (!\VGA1|px_counter\(29) & (!\VGA1|px_counter\(27) & !\VGA1|px_counter\(28)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(30),
	datab => \VGA1|ALT_INV_px_counter\(29),
	datac => \VGA1|ALT_INV_px_counter\(27),
	datad => \VGA1|ALT_INV_px_counter\(28),
	datae => \VGA1|ALT_INV_px_counter\(25),
	dataf => \VGA1|ALT_INV_px_counter\(26),
	combout => \VGA1|LessThan4~1_combout\);

-- Location: FF_X27_Y31_N47
\VGA1|px_counter[18]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~73_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter[18]~DUPLICATE_q\);

-- Location: FF_X27_Y31_N44
\VGA1|px_counter[19]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~69_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter[19]~DUPLICATE_q\);

-- Location: FF_X27_Y31_N8
\VGA1|px_counter[21]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[21]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter[21]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y31_N48
\VGA1|LessThan4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~2_combout\ = ( !\VGA1|px_counter\(17) & ( !\VGA1|px_counter[21]~DUPLICATE_q\ & ( (!\VGA1|px_counter\(20) & (!\VGA1|px_counter[18]~DUPLICATE_q\ & (!\VGA1|px_counter\(22) & !\VGA1|px_counter[19]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(20),
	datab => \VGA1|ALT_INV_px_counter[18]~DUPLICATE_q\,
	datac => \VGA1|ALT_INV_px_counter\(22),
	datad => \VGA1|ALT_INV_px_counter[19]~DUPLICATE_q\,
	datae => \VGA1|ALT_INV_px_counter\(17),
	dataf => \VGA1|ALT_INV_px_counter[21]~DUPLICATE_q\,
	combout => \VGA1|LessThan4~2_combout\);

-- Location: LABCELL_X29_Y31_N57
\VGA1|LessThan4~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~4_combout\ = ( \VGA1|LessThan4~2_combout\ & ( (\VGA1|LessThan4~3_combout\ & (\VGA1|LessThan4~0_combout\ & \VGA1|LessThan4~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan4~3_combout\,
	datac => \VGA1|ALT_INV_LessThan4~0_combout\,
	datad => \VGA1|ALT_INV_LessThan4~1_combout\,
	dataf => \VGA1|ALT_INV_LessThan4~2_combout\,
	combout => \VGA1|LessThan4~4_combout\);

-- Location: LABCELL_X29_Y31_N6
\VGA1|px_counter[22]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[22]~0_combout\ = (!\VGA1|px_counter\(31) & ((!\VGA1|LessThan4~4_combout\) # ((!\VGA1|LessThan4~7_combout\ & \VGA1|px_counter\(10)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111000000000110011100000000011001110000000001100111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan4~7_combout\,
	datab => \VGA1|ALT_INV_LessThan4~4_combout\,
	datac => \VGA1|ALT_INV_px_counter\(10),
	datad => \VGA1|ALT_INV_px_counter\(31),
	combout => \VGA1|px_counter[22]~0_combout\);

-- Location: FF_X28_Y31_N1
\VGA1|px_counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|Add1~117_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(0));

-- Location: MLABCELL_X28_Y31_N3
\VGA1|Add1~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~121_sumout\ = SUM(( \VGA1|px_counter\(1) ) + ( GND ) + ( \VGA1|Add1~118\ ))
-- \VGA1|Add1~122\ = CARRY(( \VGA1|px_counter\(1) ) + ( GND ) + ( \VGA1|Add1~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(1),
	cin => \VGA1|Add1~118\,
	sumout => \VGA1|Add1~121_sumout\,
	cout => \VGA1|Add1~122\);

-- Location: LABCELL_X29_Y31_N21
\VGA1|px_counter[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[1]~feeder_combout\ = \VGA1|Add1~121_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_Add1~121_sumout\,
	combout => \VGA1|px_counter[1]~feeder_combout\);

-- Location: FF_X29_Y31_N22
\VGA1|px_counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[1]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(1));

-- Location: MLABCELL_X28_Y31_N6
\VGA1|Add1~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~125_sumout\ = SUM(( \VGA1|px_counter\(2) ) + ( GND ) + ( \VGA1|Add1~122\ ))
-- \VGA1|Add1~126\ = CARRY(( \VGA1|px_counter\(2) ) + ( GND ) + ( \VGA1|Add1~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(2),
	cin => \VGA1|Add1~122\,
	sumout => \VGA1|Add1~125_sumout\,
	cout => \VGA1|Add1~126\);

-- Location: FF_X29_Y31_N26
\VGA1|px_counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~125_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(2));

-- Location: MLABCELL_X28_Y31_N9
\VGA1|Add1~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~109_sumout\ = SUM(( \VGA1|px_counter\(3) ) + ( GND ) + ( \VGA1|Add1~126\ ))
-- \VGA1|Add1~110\ = CARRY(( \VGA1|px_counter\(3) ) + ( GND ) + ( \VGA1|Add1~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(3),
	cin => \VGA1|Add1~126\,
	sumout => \VGA1|Add1~109_sumout\,
	cout => \VGA1|Add1~110\);

-- Location: LABCELL_X29_Y31_N27
\VGA1|px_counter[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[3]~feeder_combout\ = ( \VGA1|Add1~109_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~109_sumout\,
	combout => \VGA1|px_counter[3]~feeder_combout\);

-- Location: FF_X29_Y31_N28
\VGA1|px_counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[3]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(3));

-- Location: MLABCELL_X28_Y31_N12
\VGA1|Add1~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~113_sumout\ = SUM(( \VGA1|px_counter\(4) ) + ( GND ) + ( \VGA1|Add1~110\ ))
-- \VGA1|Add1~114\ = CARRY(( \VGA1|px_counter\(4) ) + ( GND ) + ( \VGA1|Add1~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(4),
	cin => \VGA1|Add1~110\,
	sumout => \VGA1|Add1~113_sumout\,
	cout => \VGA1|Add1~114\);

-- Location: FF_X29_Y31_N47
\VGA1|px_counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~113_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(4));

-- Location: MLABCELL_X28_Y31_N15
\VGA1|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~9_sumout\ = SUM(( \VGA1|px_counter\(5) ) + ( GND ) + ( \VGA1|Add1~114\ ))
-- \VGA1|Add1~10\ = CARRY(( \VGA1|px_counter\(5) ) + ( GND ) + ( \VGA1|Add1~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(5),
	cin => \VGA1|Add1~114\,
	sumout => \VGA1|Add1~9_sumout\,
	cout => \VGA1|Add1~10\);

-- Location: FF_X29_Y31_N35
\VGA1|px_counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~9_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(5));

-- Location: MLABCELL_X28_Y31_N18
\VGA1|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~13_sumout\ = SUM(( \VGA1|px_counter\(6) ) + ( GND ) + ( \VGA1|Add1~10\ ))
-- \VGA1|Add1~14\ = CARRY(( \VGA1|px_counter\(6) ) + ( GND ) + ( \VGA1|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(6),
	cin => \VGA1|Add1~10\,
	sumout => \VGA1|Add1~13_sumout\,
	cout => \VGA1|Add1~14\);

-- Location: FF_X29_Y31_N16
\VGA1|px_counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~13_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(6));

-- Location: MLABCELL_X28_Y31_N21
\VGA1|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~17_sumout\ = SUM(( \VGA1|px_counter\(7) ) + ( GND ) + ( \VGA1|Add1~14\ ))
-- \VGA1|Add1~18\ = CARRY(( \VGA1|px_counter\(7) ) + ( GND ) + ( \VGA1|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(7),
	cin => \VGA1|Add1~14\,
	sumout => \VGA1|Add1~17_sumout\,
	cout => \VGA1|Add1~18\);

-- Location: FF_X29_Y31_N41
\VGA1|px_counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~17_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(7));

-- Location: MLABCELL_X28_Y31_N24
\VGA1|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~1_sumout\ = SUM(( \VGA1|px_counter\(8) ) + ( GND ) + ( \VGA1|Add1~18\ ))
-- \VGA1|Add1~2\ = CARRY(( \VGA1|px_counter\(8) ) + ( GND ) + ( \VGA1|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(8),
	cin => \VGA1|Add1~18\,
	sumout => \VGA1|Add1~1_sumout\,
	cout => \VGA1|Add1~2\);

-- Location: FF_X29_Y31_N32
\VGA1|px_counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~1_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(8));

-- Location: MLABCELL_X28_Y31_N27
\VGA1|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~5_sumout\ = SUM(( \VGA1|px_counter\(9) ) + ( GND ) + ( \VGA1|Add1~2\ ))
-- \VGA1|Add1~6\ = CARRY(( \VGA1|px_counter\(9) ) + ( GND ) + ( \VGA1|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(9),
	cin => \VGA1|Add1~2\,
	sumout => \VGA1|Add1~5_sumout\,
	cout => \VGA1|Add1~6\);

-- Location: FF_X29_Y31_N38
\VGA1|px_counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~5_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(9));

-- Location: MLABCELL_X28_Y31_N30
\VGA1|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~21_sumout\ = SUM(( \VGA1|px_counter\(10) ) + ( GND ) + ( \VGA1|Add1~6\ ))
-- \VGA1|Add1~22\ = CARRY(( \VGA1|px_counter\(10) ) + ( GND ) + ( \VGA1|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(10),
	cin => \VGA1|Add1~6\,
	sumout => \VGA1|Add1~21_sumout\,
	cout => \VGA1|Add1~22\);

-- Location: FF_X29_Y31_N14
\VGA1|px_counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~21_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(10));

-- Location: MLABCELL_X28_Y31_N33
\VGA1|Add1~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~97_sumout\ = SUM(( \VGA1|px_counter\(11) ) + ( GND ) + ( \VGA1|Add1~22\ ))
-- \VGA1|Add1~98\ = CARRY(( \VGA1|px_counter\(11) ) + ( GND ) + ( \VGA1|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(11),
	cin => \VGA1|Add1~22\,
	sumout => \VGA1|Add1~97_sumout\,
	cout => \VGA1|Add1~98\);

-- Location: FF_X29_Y31_N4
\VGA1|px_counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~97_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(11));

-- Location: MLABCELL_X28_Y31_N36
\VGA1|Add1~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~101_sumout\ = SUM(( \VGA1|px_counter\(12) ) + ( GND ) + ( \VGA1|Add1~98\ ))
-- \VGA1|Add1~102\ = CARRY(( \VGA1|px_counter\(12) ) + ( GND ) + ( \VGA1|Add1~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(12),
	cin => \VGA1|Add1~98\,
	sumout => \VGA1|Add1~101_sumout\,
	cout => \VGA1|Add1~102\);

-- Location: FF_X29_Y31_N2
\VGA1|px_counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~101_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(12));

-- Location: MLABCELL_X28_Y31_N39
\VGA1|Add1~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~93_sumout\ = SUM(( \VGA1|px_counter\(13) ) + ( GND ) + ( \VGA1|Add1~102\ ))
-- \VGA1|Add1~94\ = CARRY(( \VGA1|px_counter\(13) ) + ( GND ) + ( \VGA1|Add1~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(13),
	cin => \VGA1|Add1~102\,
	sumout => \VGA1|Add1~93_sumout\,
	cout => \VGA1|Add1~94\);

-- Location: LABCELL_X27_Y31_N57
\VGA1|px_counter[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[13]~feeder_combout\ = ( \VGA1|Add1~93_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~93_sumout\,
	combout => \VGA1|px_counter[13]~feeder_combout\);

-- Location: FF_X27_Y31_N58
\VGA1|px_counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[13]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(13));

-- Location: MLABCELL_X28_Y31_N42
\VGA1|Add1~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~89_sumout\ = SUM(( \VGA1|px_counter\(14) ) + ( GND ) + ( \VGA1|Add1~94\ ))
-- \VGA1|Add1~90\ = CARRY(( \VGA1|px_counter\(14) ) + ( GND ) + ( \VGA1|Add1~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(14),
	cin => \VGA1|Add1~94\,
	sumout => \VGA1|Add1~89_sumout\,
	cout => \VGA1|Add1~90\);

-- Location: FF_X27_Y31_N55
\VGA1|px_counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~89_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(14));

-- Location: MLABCELL_X28_Y31_N45
\VGA1|Add1~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~85_sumout\ = SUM(( \VGA1|px_counter\(15) ) + ( GND ) + ( \VGA1|Add1~90\ ))
-- \VGA1|Add1~86\ = CARRY(( \VGA1|px_counter\(15) ) + ( GND ) + ( \VGA1|Add1~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(15),
	cin => \VGA1|Add1~90\,
	sumout => \VGA1|Add1~85_sumout\,
	cout => \VGA1|Add1~86\);

-- Location: FF_X29_Y31_N29
\VGA1|px_counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~85_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(15));

-- Location: MLABCELL_X28_Y31_N48
\VGA1|Add1~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~81_sumout\ = SUM(( \VGA1|px_counter\(16) ) + ( GND ) + ( \VGA1|Add1~86\ ))
-- \VGA1|Add1~82\ = CARRY(( \VGA1|px_counter\(16) ) + ( GND ) + ( \VGA1|Add1~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(16),
	cin => \VGA1|Add1~86\,
	sumout => \VGA1|Add1~81_sumout\,
	cout => \VGA1|Add1~82\);

-- Location: FF_X29_Y31_N23
\VGA1|px_counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~81_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(16));

-- Location: MLABCELL_X28_Y31_N51
\VGA1|Add1~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~77_sumout\ = SUM(( \VGA1|px_counter\(17) ) + ( GND ) + ( \VGA1|Add1~82\ ))
-- \VGA1|Add1~78\ = CARRY(( \VGA1|px_counter\(17) ) + ( GND ) + ( \VGA1|Add1~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(17),
	cin => \VGA1|Add1~82\,
	sumout => \VGA1|Add1~77_sumout\,
	cout => \VGA1|Add1~78\);

-- Location: FF_X27_Y31_N49
\VGA1|px_counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~77_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(17));

-- Location: MLABCELL_X28_Y31_N54
\VGA1|Add1~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~73_sumout\ = SUM(( \VGA1|px_counter\(18) ) + ( GND ) + ( \VGA1|Add1~78\ ))
-- \VGA1|Add1~74\ = CARRY(( \VGA1|px_counter\(18) ) + ( GND ) + ( \VGA1|Add1~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(18),
	cin => \VGA1|Add1~78\,
	sumout => \VGA1|Add1~73_sumout\,
	cout => \VGA1|Add1~74\);

-- Location: FF_X27_Y31_N46
\VGA1|px_counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~73_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(18));

-- Location: MLABCELL_X28_Y31_N57
\VGA1|Add1~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~69_sumout\ = SUM(( \VGA1|px_counter\(19) ) + ( GND ) + ( \VGA1|Add1~74\ ))
-- \VGA1|Add1~70\ = CARRY(( \VGA1|px_counter\(19) ) + ( GND ) + ( \VGA1|Add1~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(19),
	cin => \VGA1|Add1~74\,
	sumout => \VGA1|Add1~69_sumout\,
	cout => \VGA1|Add1~70\);

-- Location: FF_X27_Y31_N43
\VGA1|px_counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~69_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(19));

-- Location: MLABCELL_X28_Y30_N0
\VGA1|Add1~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~65_sumout\ = SUM(( \VGA1|px_counter\(20) ) + ( GND ) + ( \VGA1|Add1~70\ ))
-- \VGA1|Add1~66\ = CARRY(( \VGA1|px_counter\(20) ) + ( GND ) + ( \VGA1|Add1~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(20),
	cin => \VGA1|Add1~70\,
	sumout => \VGA1|Add1~65_sumout\,
	cout => \VGA1|Add1~66\);

-- Location: LABCELL_X27_Y31_N9
\VGA1|px_counter[20]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[20]~feeder_combout\ = ( \VGA1|Add1~65_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~65_sumout\,
	combout => \VGA1|px_counter[20]~feeder_combout\);

-- Location: FF_X27_Y31_N10
\VGA1|px_counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[20]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(20));

-- Location: MLABCELL_X28_Y30_N3
\VGA1|Add1~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~61_sumout\ = SUM(( \VGA1|px_counter\(21) ) + ( GND ) + ( \VGA1|Add1~66\ ))
-- \VGA1|Add1~62\ = CARRY(( \VGA1|px_counter\(21) ) + ( GND ) + ( \VGA1|Add1~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(21),
	cin => \VGA1|Add1~66\,
	sumout => \VGA1|Add1~61_sumout\,
	cout => \VGA1|Add1~62\);

-- Location: LABCELL_X27_Y31_N6
\VGA1|px_counter[21]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[21]~feeder_combout\ = ( \VGA1|Add1~61_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~61_sumout\,
	combout => \VGA1|px_counter[21]~feeder_combout\);

-- Location: FF_X27_Y31_N7
\VGA1|px_counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[21]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(21));

-- Location: MLABCELL_X28_Y30_N6
\VGA1|Add1~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~57_sumout\ = SUM(( \VGA1|px_counter\(22) ) + ( GND ) + ( \VGA1|Add1~62\ ))
-- \VGA1|Add1~58\ = CARRY(( \VGA1|px_counter\(22) ) + ( GND ) + ( \VGA1|Add1~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(22),
	cin => \VGA1|Add1~62\,
	sumout => \VGA1|Add1~57_sumout\,
	cout => \VGA1|Add1~58\);

-- Location: LABCELL_X27_Y31_N30
\VGA1|px_counter[22]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[22]~feeder_combout\ = ( \VGA1|Add1~57_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~57_sumout\,
	combout => \VGA1|px_counter[22]~feeder_combout\);

-- Location: FF_X27_Y31_N31
\VGA1|px_counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[22]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(22));

-- Location: MLABCELL_X28_Y30_N9
\VGA1|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~29_sumout\ = SUM(( \VGA1|px_counter\(23) ) + ( GND ) + ( \VGA1|Add1~58\ ))
-- \VGA1|Add1~30\ = CARRY(( \VGA1|px_counter\(23) ) + ( GND ) + ( \VGA1|Add1~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(23),
	cin => \VGA1|Add1~58\,
	sumout => \VGA1|Add1~29_sumout\,
	cout => \VGA1|Add1~30\);

-- Location: FF_X28_Y30_N44
\VGA1|px_counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~29_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(23));

-- Location: MLABCELL_X28_Y30_N12
\VGA1|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~25_sumout\ = SUM(( \VGA1|px_counter\(24) ) + ( GND ) + ( \VGA1|Add1~30\ ))
-- \VGA1|Add1~26\ = CARRY(( \VGA1|px_counter\(24) ) + ( GND ) + ( \VGA1|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(24),
	cin => \VGA1|Add1~30\,
	sumout => \VGA1|Add1~25_sumout\,
	cout => \VGA1|Add1~26\);

-- Location: MLABCELL_X28_Y30_N36
\VGA1|px_counter[24]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[24]~feeder_combout\ = ( \VGA1|Add1~25_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~25_sumout\,
	combout => \VGA1|px_counter[24]~feeder_combout\);

-- Location: FF_X28_Y30_N38
\VGA1|px_counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[24]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(24));

-- Location: MLABCELL_X28_Y30_N15
\VGA1|Add1~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~53_sumout\ = SUM(( \VGA1|px_counter\(25) ) + ( GND ) + ( \VGA1|Add1~26\ ))
-- \VGA1|Add1~54\ = CARRY(( \VGA1|px_counter\(25) ) + ( GND ) + ( \VGA1|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(25),
	cin => \VGA1|Add1~26\,
	sumout => \VGA1|Add1~53_sumout\,
	cout => \VGA1|Add1~54\);

-- Location: FF_X28_Y30_N50
\VGA1|px_counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~53_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(25));

-- Location: MLABCELL_X28_Y30_N18
\VGA1|Add1~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~49_sumout\ = SUM(( \VGA1|px_counter\(26) ) + ( GND ) + ( \VGA1|Add1~54\ ))
-- \VGA1|Add1~50\ = CARRY(( \VGA1|px_counter\(26) ) + ( GND ) + ( \VGA1|Add1~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(26),
	cin => \VGA1|Add1~54\,
	sumout => \VGA1|Add1~49_sumout\,
	cout => \VGA1|Add1~50\);

-- Location: MLABCELL_X28_Y30_N39
\VGA1|px_counter[26]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[26]~feeder_combout\ = ( \VGA1|Add1~49_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~49_sumout\,
	combout => \VGA1|px_counter[26]~feeder_combout\);

-- Location: FF_X28_Y30_N41
\VGA1|px_counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[26]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(26));

-- Location: MLABCELL_X28_Y30_N21
\VGA1|Add1~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~45_sumout\ = SUM(( \VGA1|px_counter\(27) ) + ( GND ) + ( \VGA1|Add1~50\ ))
-- \VGA1|Add1~46\ = CARRY(( \VGA1|px_counter\(27) ) + ( GND ) + ( \VGA1|Add1~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(27),
	cin => \VGA1|Add1~50\,
	sumout => \VGA1|Add1~45_sumout\,
	cout => \VGA1|Add1~46\);

-- Location: FF_X28_Y30_N56
\VGA1|px_counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~45_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(27));

-- Location: MLABCELL_X28_Y30_N24
\VGA1|Add1~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~41_sumout\ = SUM(( \VGA1|px_counter\(28) ) + ( GND ) + ( \VGA1|Add1~46\ ))
-- \VGA1|Add1~42\ = CARRY(( \VGA1|px_counter\(28) ) + ( GND ) + ( \VGA1|Add1~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(28),
	cin => \VGA1|Add1~46\,
	sumout => \VGA1|Add1~41_sumout\,
	cout => \VGA1|Add1~42\);

-- Location: MLABCELL_X28_Y30_N57
\VGA1|px_counter[28]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|px_counter[28]~feeder_combout\ = ( \VGA1|Add1~41_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add1~41_sumout\,
	combout => \VGA1|px_counter[28]~feeder_combout\);

-- Location: FF_X28_Y30_N59
\VGA1|px_counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|px_counter[28]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(28));

-- Location: MLABCELL_X28_Y30_N27
\VGA1|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~37_sumout\ = SUM(( \VGA1|px_counter\(29) ) + ( GND ) + ( \VGA1|Add1~42\ ))
-- \VGA1|Add1~38\ = CARRY(( \VGA1|px_counter\(29) ) + ( GND ) + ( \VGA1|Add1~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(29),
	cin => \VGA1|Add1~42\,
	sumout => \VGA1|Add1~37_sumout\,
	cout => \VGA1|Add1~38\);

-- Location: FF_X28_Y30_N47
\VGA1|px_counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~37_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(29));

-- Location: MLABCELL_X28_Y30_N30
\VGA1|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~33_sumout\ = SUM(( \VGA1|px_counter\(30) ) + ( GND ) + ( \VGA1|Add1~38\ ))
-- \VGA1|Add1~34\ = CARRY(( \VGA1|px_counter\(30) ) + ( GND ) + ( \VGA1|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_px_counter\(30),
	cin => \VGA1|Add1~38\,
	sumout => \VGA1|Add1~33_sumout\,
	cout => \VGA1|Add1~34\);

-- Location: FF_X28_Y30_N53
\VGA1|px_counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~33_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(30));

-- Location: MLABCELL_X28_Y30_N33
\VGA1|Add1~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add1~105_sumout\ = SUM(( \VGA1|px_counter\(31) ) + ( GND ) + ( \VGA1|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(31),
	cin => \VGA1|Add1~34\,
	sumout => \VGA1|Add1~105_sumout\);

-- Location: FF_X29_Y31_N8
\VGA1|px_counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add1~105_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|px_counter[22]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|px_counter\(31));

-- Location: LABCELL_X29_Y31_N36
\VGA1|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan0~0_combout\ = ( \VGA1|px_counter\(6) & ( (\VGA1|px_counter\(8) & \VGA1|px_counter\(9)) ) ) # ( !\VGA1|px_counter\(6) & ( (\VGA1|px_counter\(8) & (\VGA1|px_counter\(9) & ((\VGA1|px_counter\(5)) # (\VGA1|px_counter\(7))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010011000000000001001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(7),
	datab => \VGA1|ALT_INV_px_counter\(8),
	datac => \VGA1|ALT_INV_px_counter\(5),
	datad => \VGA1|ALT_INV_px_counter\(9),
	dataf => \VGA1|ALT_INV_px_counter\(6),
	combout => \VGA1|LessThan0~0_combout\);

-- Location: LABCELL_X29_Y31_N12
\VGA1|LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan0~1_combout\ = ( !\VGA1|px_counter\(10) & ( \VGA1|LessThan4~2_combout\ & ( (\VGA1|LessThan4~1_combout\ & (\VGA1|LessThan4~0_combout\ & (\VGA1|LessThan4~3_combout\ & !\VGA1|LessThan0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan4~1_combout\,
	datab => \VGA1|ALT_INV_LessThan4~0_combout\,
	datac => \VGA1|ALT_INV_LessThan4~3_combout\,
	datad => \VGA1|ALT_INV_LessThan0~0_combout\,
	datae => \VGA1|ALT_INV_px_counter\(10),
	dataf => \VGA1|ALT_INV_LessThan4~2_combout\,
	combout => \VGA1|LessThan0~1_combout\);

-- Location: LABCELL_X29_Y31_N51
\VGA1|LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan0~2_combout\ = ( !\VGA1|LessThan0~1_combout\ & ( !\VGA1|px_counter\(31) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(31),
	dataf => \VGA1|ALT_INV_LessThan0~1_combout\,
	combout => \VGA1|LessThan0~2_combout\);

-- Location: MLABCELL_X28_Y33_N0
\VGA1|Add2~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~125_sumout\ = SUM(( \VGA1|line_counter\(0) ) + ( VCC ) + ( !VCC ))
-- \VGA1|Add2~126\ = CARRY(( \VGA1|line_counter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \VGA1|ALT_INV_line_counter\(0),
	cin => GND,
	sumout => \VGA1|Add2~125_sumout\,
	cout => \VGA1|Add2~126\);

-- Location: LABCELL_X27_Y33_N18
\VGA1|LessThan1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan1~0_combout\ = (\VGA1|line_counter\(6) & (((\VGA1|line_counter\(4) & \VGA1|line_counter\(3))) # (\VGA1|line_counter\(5))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010101000100010001010100010001000101010001000100010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(6),
	datab => \VGA1|ALT_INV_line_counter\(5),
	datac => \VGA1|ALT_INV_line_counter\(4),
	datad => \VGA1|ALT_INV_line_counter\(3),
	combout => \VGA1|LessThan1~0_combout\);

-- Location: LABCELL_X27_Y33_N45
\VGA1|LessThan5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan5~0_combout\ = ( !\VGA1|line_counter\(5) & ( (!\VGA1|line_counter\(2) & (!\VGA1|line_counter\(0) & !\VGA1|line_counter\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100000000000100010000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(2),
	datab => \VGA1|ALT_INV_line_counter\(0),
	datad => \VGA1|ALT_INV_line_counter\(1),
	dataf => \VGA1|ALT_INV_line_counter\(5),
	combout => \VGA1|LessThan5~0_combout\);

-- Location: LABCELL_X27_Y33_N3
\VGA1|LessThan5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan5~1_combout\ = ( \VGA1|line_counter\(9) & ( (((\VGA1|LessThan1~0_combout\ & !\VGA1|LessThan5~0_combout\)) # (\VGA1|line_counter\(8))) # (\VGA1|line_counter\(7)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001110011111111110111001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan1~0_combout\,
	datab => \VGA1|ALT_INV_line_counter\(7),
	datac => \VGA1|ALT_INV_LessThan5~0_combout\,
	datad => \VGA1|ALT_INV_line_counter\(8),
	dataf => \VGA1|ALT_INV_line_counter\(9),
	combout => \VGA1|LessThan5~1_combout\);

-- Location: MLABCELL_X28_Y32_N48
\VGA1|PX_ADDR[20]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~2_combout\ = ( !\VGA1|line_counter\(27) & ( !\VGA1|line_counter\(30) & ( (!\VGA1|line_counter\(28) & (!\VGA1|line_counter\(29) & !\VGA1|line_counter\(10))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(28),
	datab => \VGA1|ALT_INV_line_counter\(29),
	datac => \VGA1|ALT_INV_line_counter\(10),
	datae => \VGA1|ALT_INV_line_counter\(27),
	dataf => \VGA1|ALT_INV_line_counter\(30),
	combout => \VGA1|PX_ADDR[20]~2_combout\);

-- Location: MLABCELL_X28_Y32_N42
\VGA1|PX_ADDR[20]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~3_combout\ = ( !\VGA1|line_counter\(21) & ( !\VGA1|line_counter\(22) & ( (!\VGA1|line_counter\(23) & (!\VGA1|line_counter\(24) & !\VGA1|line_counter\(25))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(23),
	datab => \VGA1|ALT_INV_line_counter\(24),
	datac => \VGA1|ALT_INV_line_counter\(25),
	datae => \VGA1|ALT_INV_line_counter\(21),
	dataf => \VGA1|ALT_INV_line_counter\(22),
	combout => \VGA1|PX_ADDR[20]~3_combout\);

-- Location: LABCELL_X29_Y31_N9
\VGA1|VGA_VS~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_VS~1_combout\ = (!\VGA1|px_counter\(31) & ((!\VGA1|LessThan4~4_combout\) # ((!\VGA1|LessThan4~7_combout\ & \VGA1|px_counter\(10)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111000000000110011100000000011001110000000001100111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan4~7_combout\,
	datab => \VGA1|ALT_INV_LessThan4~4_combout\,
	datac => \VGA1|ALT_INV_px_counter\(10),
	datad => \VGA1|ALT_INV_px_counter\(31),
	combout => \VGA1|VGA_VS~1_combout\);

-- Location: FF_X29_Y33_N5
\VGA1|line_counter[15]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[15]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter[15]~DUPLICATE_q\);

-- Location: LABCELL_X29_Y33_N42
\VGA1|PX_ADDR[20]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~1_combout\ = ( !\VGA1|line_counter\(14) & ( !\VGA1|line_counter\(19) & ( (!\VGA1|line_counter\(16) & (!\VGA1|line_counter\(17) & (!\VGA1|line_counter[15]~DUPLICATE_q\ & !\VGA1|line_counter\(18)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(16),
	datab => \VGA1|ALT_INV_line_counter\(17),
	datac => \VGA1|ALT_INV_line_counter[15]~DUPLICATE_q\,
	datad => \VGA1|ALT_INV_line_counter\(18),
	datae => \VGA1|ALT_INV_line_counter\(14),
	dataf => \VGA1|ALT_INV_line_counter\(19),
	combout => \VGA1|PX_ADDR[20]~1_combout\);

-- Location: LABCELL_X29_Y33_N39
\VGA1|PX_ADDR[20]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~0_combout\ = ( !\VGA1|line_counter\(13) & ( (!\VGA1|line_counter\(11) & !\VGA1|line_counter\(12)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(11),
	datad => \VGA1|ALT_INV_line_counter\(12),
	dataf => \VGA1|ALT_INV_line_counter\(13),
	combout => \VGA1|PX_ADDR[20]~0_combout\);

-- Location: MLABCELL_X28_Y32_N36
\VGA1|PX_ADDR[20]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~4_combout\ = ( !\VGA1|line_counter\(20) & ( \VGA1|PX_ADDR[20]~0_combout\ & ( (\VGA1|PX_ADDR[20]~2_combout\ & (\VGA1|PX_ADDR[20]~3_combout\ & (!\VGA1|line_counter\(26) & \VGA1|PX_ADDR[20]~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR[20]~2_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR[20]~3_combout\,
	datac => \VGA1|ALT_INV_line_counter\(26),
	datad => \VGA1|ALT_INV_PX_ADDR[20]~1_combout\,
	datae => \VGA1|ALT_INV_line_counter\(20),
	dataf => \VGA1|ALT_INV_PX_ADDR[20]~0_combout\,
	combout => \VGA1|PX_ADDR[20]~4_combout\);

-- Location: LABCELL_X27_Y33_N0
\VGA1|LessThan1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan1~1_combout\ = (\VGA1|line_counter\(9) & (((\VGA1|line_counter\(8)) # (\VGA1|line_counter\(7))) # (\VGA1|LessThan1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001111111000000000111111100000000011111110000000001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan1~0_combout\,
	datab => \VGA1|ALT_INV_line_counter\(7),
	datac => \VGA1|ALT_INV_line_counter\(8),
	datad => \VGA1|ALT_INV_line_counter\(9),
	combout => \VGA1|LessThan1~1_combout\);

-- Location: LABCELL_X27_Y33_N42
\VGA1|LessThan6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan6~0_combout\ = ( \VGA1|line_counter\(1) & ( (!\VGA1|line_counter\(2) & !\VGA1|line_counter\(5)) ) ) # ( !\VGA1|line_counter\(1) & ( (!\VGA1|line_counter\(5) & ((!\VGA1|line_counter\(2)) # (!\VGA1|line_counter\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111000000000111011100000000010101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(2),
	datab => \VGA1|ALT_INV_line_counter\(0),
	datad => \VGA1|ALT_INV_line_counter\(5),
	dataf => \VGA1|ALT_INV_line_counter\(1),
	combout => \VGA1|LessThan6~0_combout\);

-- Location: LABCELL_X27_Y33_N21
\VGA1|LessThan7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan7~1_combout\ = (\VGA1|line_counter\(6) & (\VGA1|line_counter\(5) & \VGA1|line_counter\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010001000000000001000100000000000100010000000000010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(6),
	datab => \VGA1|ALT_INV_line_counter\(5),
	datad => \VGA1|ALT_INV_line_counter\(4),
	combout => \VGA1|LessThan7~1_combout\);

-- Location: LABCELL_X27_Y33_N30
\VGA1|LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan7~0_combout\ = ( !\VGA1|line_counter\(2) & ( \VGA1|line_counter\(0) & ( (!\VGA1|line_counter\(3) & !\VGA1|line_counter\(1)) ) ) ) # ( !\VGA1|line_counter\(2) & ( !\VGA1|line_counter\(0) & ( !\VGA1|line_counter\(3) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010000000000000000010100000101000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(3),
	datac => \VGA1|ALT_INV_line_counter\(1),
	datae => \VGA1|ALT_INV_line_counter\(2),
	dataf => \VGA1|ALT_INV_line_counter\(0),
	combout => \VGA1|LessThan7~0_combout\);

-- Location: LABCELL_X27_Y33_N24
\VGA1|PX_ADDR[20]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~6_combout\ = ( !\VGA1|line_counter\(7) & ( \VGA1|LessThan7~0_combout\ & ( !\VGA1|line_counter\(8) ) ) ) # ( !\VGA1|line_counter\(7) & ( !\VGA1|LessThan7~0_combout\ & ( (!\VGA1|line_counter\(8) & ((!\VGA1|LessThan1~0_combout\) # 
-- ((!\VGA1|LessThan7~1_combout\) # (\VGA1|LessThan6~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000010110000000000000000000011110000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan1~0_combout\,
	datab => \VGA1|ALT_INV_LessThan6~0_combout\,
	datac => \VGA1|ALT_INV_line_counter\(8),
	datad => \VGA1|ALT_INV_LessThan7~1_combout\,
	datae => \VGA1|ALT_INV_line_counter\(7),
	dataf => \VGA1|ALT_INV_LessThan7~0_combout\,
	combout => \VGA1|PX_ADDR[20]~6_combout\);

-- Location: LABCELL_X27_Y33_N48
\VGA1|PX_ADDR[20]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~7_combout\ = ( \VGA1|PX_ADDR[20]~6_combout\ & ( (!\VGA1|PX_ADDR[20]~4_combout\ & !\VGA1|line_counter\(31)) ) ) # ( !\VGA1|PX_ADDR[20]~6_combout\ & ( (!\VGA1|line_counter\(31) & ((!\VGA1|PX_ADDR[20]~4_combout\) # 
-- ((\VGA1|LessThan5~1_combout\ & \VGA1|LessThan1~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110100000000110011010000000011001100000000001100110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan5~1_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR[20]~4_combout\,
	datac => \VGA1|ALT_INV_LessThan1~1_combout\,
	datad => \VGA1|ALT_INV_line_counter\(31),
	dataf => \VGA1|ALT_INV_PX_ADDR[20]~6_combout\,
	combout => \VGA1|PX_ADDR[20]~7_combout\);

-- Location: FF_X28_Y33_N1
\VGA1|line_counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|Add2~125_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(0));

-- Location: MLABCELL_X28_Y33_N3
\VGA1|Add2~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~121_sumout\ = SUM(( \VGA1|line_counter\(1) ) + ( GND ) + ( \VGA1|Add2~126\ ))
-- \VGA1|Add2~122\ = CARRY(( \VGA1|line_counter\(1) ) + ( GND ) + ( \VGA1|Add2~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(1),
	cin => \VGA1|Add2~126\,
	sumout => \VGA1|Add2~121_sumout\,
	cout => \VGA1|Add2~122\);

-- Location: FF_X27_Y33_N47
\VGA1|line_counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~121_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(1));

-- Location: MLABCELL_X28_Y33_N6
\VGA1|Add2~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~117_sumout\ = SUM(( \VGA1|line_counter\(2) ) + ( GND ) + ( \VGA1|Add2~122\ ))
-- \VGA1|Add2~118\ = CARRY(( \VGA1|line_counter\(2) ) + ( GND ) + ( \VGA1|Add2~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(2),
	cin => \VGA1|Add2~122\,
	sumout => \VGA1|Add2~117_sumout\,
	cout => \VGA1|Add2~118\);

-- Location: FF_X27_Y33_N32
\VGA1|line_counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~117_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(2));

-- Location: MLABCELL_X28_Y33_N9
\VGA1|Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~17_sumout\ = SUM(( \VGA1|line_counter\(3) ) + ( GND ) + ( \VGA1|Add2~118\ ))
-- \VGA1|Add2~18\ = CARRY(( \VGA1|line_counter\(3) ) + ( GND ) + ( \VGA1|Add2~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(3),
	cin => \VGA1|Add2~118\,
	sumout => \VGA1|Add2~17_sumout\,
	cout => \VGA1|Add2~18\);

-- Location: FF_X27_Y33_N20
\VGA1|line_counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~17_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(3));

-- Location: MLABCELL_X28_Y33_N12
\VGA1|Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~13_sumout\ = SUM(( \VGA1|line_counter\(4) ) + ( GND ) + ( \VGA1|Add2~18\ ))
-- \VGA1|Add2~14\ = CARRY(( \VGA1|line_counter\(4) ) + ( GND ) + ( \VGA1|Add2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(4),
	cin => \VGA1|Add2~18\,
	sumout => \VGA1|Add2~13_sumout\,
	cout => \VGA1|Add2~14\);

-- Location: FF_X27_Y33_N23
\VGA1|line_counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~13_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(4));

-- Location: MLABCELL_X28_Y33_N15
\VGA1|Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~9_sumout\ = SUM(( \VGA1|line_counter\(5) ) + ( GND ) + ( \VGA1|Add2~14\ ))
-- \VGA1|Add2~10\ = CARRY(( \VGA1|line_counter\(5) ) + ( GND ) + ( \VGA1|Add2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(5),
	cin => \VGA1|Add2~14\,
	sumout => \VGA1|Add2~9_sumout\,
	cout => \VGA1|Add2~10\);

-- Location: FF_X27_Y33_N44
\VGA1|line_counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~9_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(5));

-- Location: MLABCELL_X28_Y33_N18
\VGA1|Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~5_sumout\ = SUM(( \VGA1|line_counter\(6) ) + ( GND ) + ( \VGA1|Add2~10\ ))
-- \VGA1|Add2~6\ = CARRY(( \VGA1|line_counter\(6) ) + ( GND ) + ( \VGA1|Add2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(6),
	cin => \VGA1|Add2~10\,
	sumout => \VGA1|Add2~5_sumout\,
	cout => \VGA1|Add2~6\);

-- Location: FF_X27_Y33_N40
\VGA1|line_counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~5_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(6));

-- Location: MLABCELL_X28_Y33_N21
\VGA1|Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~25_sumout\ = SUM(( \VGA1|line_counter\(7) ) + ( GND ) + ( \VGA1|Add2~6\ ))
-- \VGA1|Add2~26\ = CARRY(( \VGA1|line_counter\(7) ) + ( GND ) + ( \VGA1|Add2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(7),
	cin => \VGA1|Add2~6\,
	sumout => \VGA1|Add2~25_sumout\,
	cout => \VGA1|Add2~26\);

-- Location: FF_X27_Y33_N26
\VGA1|line_counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~25_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(7));

-- Location: MLABCELL_X28_Y33_N24
\VGA1|Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~21_sumout\ = SUM(( \VGA1|line_counter\(8) ) + ( GND ) + ( \VGA1|Add2~26\ ))
-- \VGA1|Add2~22\ = CARRY(( \VGA1|line_counter\(8) ) + ( GND ) + ( \VGA1|Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(8),
	cin => \VGA1|Add2~26\,
	sumout => \VGA1|Add2~21_sumout\,
	cout => \VGA1|Add2~22\);

-- Location: FF_X27_Y33_N5
\VGA1|line_counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~21_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(8));

-- Location: MLABCELL_X28_Y33_N27
\VGA1|Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~1_sumout\ = SUM(( \VGA1|line_counter\(9) ) + ( GND ) + ( \VGA1|Add2~22\ ))
-- \VGA1|Add2~2\ = CARRY(( \VGA1|line_counter\(9) ) + ( GND ) + ( \VGA1|Add2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(9),
	cin => \VGA1|Add2~22\,
	sumout => \VGA1|Add2~1_sumout\,
	cout => \VGA1|Add2~2\);

-- Location: FF_X27_Y33_N2
\VGA1|line_counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~1_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(9));

-- Location: MLABCELL_X28_Y33_N30
\VGA1|Add2~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~73_sumout\ = SUM(( \VGA1|line_counter\(10) ) + ( GND ) + ( \VGA1|Add2~2\ ))
-- \VGA1|Add2~74\ = CARRY(( \VGA1|line_counter\(10) ) + ( GND ) + ( \VGA1|Add2~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_line_counter\(10),
	cin => \VGA1|Add2~2\,
	sumout => \VGA1|Add2~73_sumout\,
	cout => \VGA1|Add2~74\);

-- Location: FF_X27_Y33_N10
\VGA1|line_counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~73_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(10));

-- Location: MLABCELL_X28_Y33_N33
\VGA1|Add2~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~45_sumout\ = SUM(( \VGA1|line_counter\(11) ) + ( GND ) + ( \VGA1|Add2~74\ ))
-- \VGA1|Add2~46\ = CARRY(( \VGA1|line_counter\(11) ) + ( GND ) + ( \VGA1|Add2~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(11),
	cin => \VGA1|Add2~74\,
	sumout => \VGA1|Add2~45_sumout\,
	cout => \VGA1|Add2~46\);

-- Location: FF_X29_Y33_N47
\VGA1|line_counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~45_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(11));

-- Location: MLABCELL_X28_Y33_N36
\VGA1|Add2~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~41_sumout\ = SUM(( \VGA1|line_counter\(12) ) + ( GND ) + ( \VGA1|Add2~46\ ))
-- \VGA1|Add2~42\ = CARRY(( \VGA1|line_counter\(12) ) + ( GND ) + ( \VGA1|Add2~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_line_counter\(12),
	cin => \VGA1|Add2~46\,
	sumout => \VGA1|Add2~41_sumout\,
	cout => \VGA1|Add2~42\);

-- Location: FF_X29_Y33_N40
\VGA1|line_counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~41_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(12));

-- Location: MLABCELL_X28_Y33_N39
\VGA1|Add2~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~37_sumout\ = SUM(( \VGA1|line_counter\(13) ) + ( GND ) + ( \VGA1|Add2~42\ ))
-- \VGA1|Add2~38\ = CARRY(( \VGA1|line_counter\(13) ) + ( GND ) + ( \VGA1|Add2~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(13),
	cin => \VGA1|Add2~42\,
	sumout => \VGA1|Add2~37_sumout\,
	cout => \VGA1|Add2~38\);

-- Location: LABCELL_X29_Y33_N33
\VGA1|line_counter[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[13]~feeder_combout\ = ( \VGA1|Add2~37_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~37_sumout\,
	combout => \VGA1|line_counter[13]~feeder_combout\);

-- Location: FF_X29_Y33_N35
\VGA1|line_counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[13]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(13));

-- Location: MLABCELL_X28_Y33_N42
\VGA1|Add2~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~69_sumout\ = SUM(( \VGA1|line_counter\(14) ) + ( GND ) + ( \VGA1|Add2~38\ ))
-- \VGA1|Add2~70\ = CARRY(( \VGA1|line_counter\(14) ) + ( GND ) + ( \VGA1|Add2~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_line_counter\(14),
	cin => \VGA1|Add2~38\,
	sumout => \VGA1|Add2~69_sumout\,
	cout => \VGA1|Add2~70\);

-- Location: FF_X29_Y33_N44
\VGA1|line_counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~69_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(14));

-- Location: MLABCELL_X28_Y33_N45
\VGA1|Add2~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~65_sumout\ = SUM(( \VGA1|line_counter\(15) ) + ( GND ) + ( \VGA1|Add2~70\ ))
-- \VGA1|Add2~66\ = CARRY(( \VGA1|line_counter\(15) ) + ( GND ) + ( \VGA1|Add2~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(15),
	cin => \VGA1|Add2~70\,
	sumout => \VGA1|Add2~65_sumout\,
	cout => \VGA1|Add2~66\);

-- Location: LABCELL_X29_Y33_N3
\VGA1|line_counter[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[15]~feeder_combout\ = ( \VGA1|Add2~65_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~65_sumout\,
	combout => \VGA1|line_counter[15]~feeder_combout\);

-- Location: FF_X29_Y33_N4
\VGA1|line_counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[15]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(15));

-- Location: MLABCELL_X28_Y33_N48
\VGA1|Add2~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~61_sumout\ = SUM(( \VGA1|line_counter\(16) ) + ( GND ) + ( \VGA1|Add2~66\ ))
-- \VGA1|Add2~62\ = CARRY(( \VGA1|line_counter\(16) ) + ( GND ) + ( \VGA1|Add2~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(16),
	cin => \VGA1|Add2~66\,
	sumout => \VGA1|Add2~61_sumout\,
	cout => \VGA1|Add2~62\);

-- Location: LABCELL_X29_Y33_N0
\VGA1|line_counter[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[16]~feeder_combout\ = ( \VGA1|Add2~61_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~61_sumout\,
	combout => \VGA1|line_counter[16]~feeder_combout\);

-- Location: FF_X29_Y33_N2
\VGA1|line_counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[16]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(16));

-- Location: MLABCELL_X28_Y33_N51
\VGA1|Add2~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~57_sumout\ = SUM(( \VGA1|line_counter\(17) ) + ( GND ) + ( \VGA1|Add2~62\ ))
-- \VGA1|Add2~58\ = CARRY(( \VGA1|line_counter\(17) ) + ( GND ) + ( \VGA1|Add2~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(17),
	cin => \VGA1|Add2~62\,
	sumout => \VGA1|Add2~57_sumout\,
	cout => \VGA1|Add2~58\);

-- Location: LABCELL_X29_Y33_N27
\VGA1|line_counter[17]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[17]~feeder_combout\ = ( \VGA1|Add2~57_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~57_sumout\,
	combout => \VGA1|line_counter[17]~feeder_combout\);

-- Location: FF_X29_Y33_N28
\VGA1|line_counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[17]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(17));

-- Location: MLABCELL_X28_Y33_N54
\VGA1|Add2~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~53_sumout\ = SUM(( \VGA1|line_counter\(18) ) + ( GND ) + ( \VGA1|Add2~58\ ))
-- \VGA1|Add2~54\ = CARRY(( \VGA1|line_counter\(18) ) + ( GND ) + ( \VGA1|Add2~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(18),
	cin => \VGA1|Add2~58\,
	sumout => \VGA1|Add2~53_sumout\,
	cout => \VGA1|Add2~54\);

-- Location: LABCELL_X29_Y33_N24
\VGA1|line_counter[18]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[18]~feeder_combout\ = ( \VGA1|Add2~53_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~53_sumout\,
	combout => \VGA1|line_counter[18]~feeder_combout\);

-- Location: FF_X29_Y33_N25
\VGA1|line_counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[18]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(18));

-- Location: MLABCELL_X28_Y33_N57
\VGA1|Add2~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~49_sumout\ = SUM(( \VGA1|line_counter\(19) ) + ( GND ) + ( \VGA1|Add2~54\ ))
-- \VGA1|Add2~50\ = CARRY(( \VGA1|line_counter\(19) ) + ( GND ) + ( \VGA1|Add2~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(19),
	cin => \VGA1|Add2~54\,
	sumout => \VGA1|Add2~49_sumout\,
	cout => \VGA1|Add2~50\);

-- Location: FF_X29_Y33_N50
\VGA1|line_counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~49_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(19));

-- Location: MLABCELL_X28_Y32_N0
\VGA1|Add2~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~33_sumout\ = SUM(( \VGA1|line_counter\(20) ) + ( GND ) + ( \VGA1|Add2~50\ ))
-- \VGA1|Add2~34\ = CARRY(( \VGA1|line_counter\(20) ) + ( GND ) + ( \VGA1|Add2~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(20),
	cin => \VGA1|Add2~50\,
	sumout => \VGA1|Add2~33_sumout\,
	cout => \VGA1|Add2~34\);

-- Location: FF_X28_Y32_N38
\VGA1|line_counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~33_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(20));

-- Location: MLABCELL_X28_Y32_N3
\VGA1|Add2~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~109_sumout\ = SUM(( \VGA1|line_counter\(21) ) + ( GND ) + ( \VGA1|Add2~34\ ))
-- \VGA1|Add2~110\ = CARRY(( \VGA1|line_counter\(21) ) + ( GND ) + ( \VGA1|Add2~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(21),
	cin => \VGA1|Add2~34\,
	sumout => \VGA1|Add2~109_sumout\,
	cout => \VGA1|Add2~110\);

-- Location: FF_X28_Y32_N43
\VGA1|line_counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~109_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(21));

-- Location: MLABCELL_X28_Y32_N6
\VGA1|Add2~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~105_sumout\ = SUM(( \VGA1|line_counter\(22) ) + ( GND ) + ( \VGA1|Add2~110\ ))
-- \VGA1|Add2~106\ = CARRY(( \VGA1|line_counter\(22) ) + ( GND ) + ( \VGA1|Add2~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(22),
	cin => \VGA1|Add2~110\,
	sumout => \VGA1|Add2~105_sumout\,
	cout => \VGA1|Add2~106\);

-- Location: FF_X27_Y33_N28
\VGA1|line_counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~105_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(22));

-- Location: MLABCELL_X28_Y32_N9
\VGA1|Add2~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~101_sumout\ = SUM(( \VGA1|line_counter\(23) ) + ( GND ) + ( \VGA1|Add2~106\ ))
-- \VGA1|Add2~102\ = CARRY(( \VGA1|line_counter\(23) ) + ( GND ) + ( \VGA1|Add2~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(23),
	cin => \VGA1|Add2~106\,
	sumout => \VGA1|Add2~101_sumout\,
	cout => \VGA1|Add2~102\);

-- Location: FF_X27_Y33_N13
\VGA1|line_counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~101_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(23));

-- Location: MLABCELL_X28_Y32_N12
\VGA1|Add2~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~97_sumout\ = SUM(( \VGA1|line_counter\(24) ) + ( GND ) + ( \VGA1|Add2~102\ ))
-- \VGA1|Add2~98\ = CARRY(( \VGA1|line_counter\(24) ) + ( GND ) + ( \VGA1|Add2~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_line_counter\(24),
	cin => \VGA1|Add2~102\,
	sumout => \VGA1|Add2~97_sumout\,
	cout => \VGA1|Add2~98\);

-- Location: MLABCELL_X28_Y32_N57
\VGA1|line_counter[24]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[24]~feeder_combout\ = ( \VGA1|Add2~97_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~97_sumout\,
	combout => \VGA1|line_counter[24]~feeder_combout\);

-- Location: FF_X28_Y32_N59
\VGA1|line_counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[24]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(24));

-- Location: MLABCELL_X28_Y32_N15
\VGA1|Add2~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~93_sumout\ = SUM(( \VGA1|line_counter\(25) ) + ( GND ) + ( \VGA1|Add2~98\ ))
-- \VGA1|Add2~94\ = CARRY(( \VGA1|line_counter\(25) ) + ( GND ) + ( \VGA1|Add2~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(25),
	cin => \VGA1|Add2~98\,
	sumout => \VGA1|Add2~93_sumout\,
	cout => \VGA1|Add2~94\);

-- Location: MLABCELL_X28_Y32_N54
\VGA1|line_counter[25]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|line_counter[25]~feeder_combout\ = ( \VGA1|Add2~93_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add2~93_sumout\,
	combout => \VGA1|line_counter[25]~feeder_combout\);

-- Location: FF_X28_Y32_N56
\VGA1|line_counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|line_counter[25]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(25));

-- Location: MLABCELL_X28_Y32_N18
\VGA1|Add2~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~29_sumout\ = SUM(( \VGA1|line_counter\(26) ) + ( GND ) + ( \VGA1|Add2~94\ ))
-- \VGA1|Add2~30\ = CARRY(( \VGA1|line_counter\(26) ) + ( GND ) + ( \VGA1|Add2~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(26),
	cin => \VGA1|Add2~94\,
	sumout => \VGA1|Add2~29_sumout\,
	cout => \VGA1|Add2~30\);

-- Location: FF_X27_Y33_N34
\VGA1|line_counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~29_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(26));

-- Location: MLABCELL_X28_Y32_N21
\VGA1|Add2~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~89_sumout\ = SUM(( \VGA1|line_counter\(27) ) + ( GND ) + ( \VGA1|Add2~30\ ))
-- \VGA1|Add2~90\ = CARRY(( \VGA1|line_counter\(27) ) + ( GND ) + ( \VGA1|Add2~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(27),
	cin => \VGA1|Add2~30\,
	sumout => \VGA1|Add2~89_sumout\,
	cout => \VGA1|Add2~90\);

-- Location: FF_X28_Y32_N49
\VGA1|line_counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~89_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(27));

-- Location: MLABCELL_X28_Y32_N24
\VGA1|Add2~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~85_sumout\ = SUM(( \VGA1|line_counter\(28) ) + ( GND ) + ( \VGA1|Add2~90\ ))
-- \VGA1|Add2~86\ = CARRY(( \VGA1|line_counter\(28) ) + ( GND ) + ( \VGA1|Add2~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(28),
	cin => \VGA1|Add2~90\,
	sumout => \VGA1|Add2~85_sumout\,
	cout => \VGA1|Add2~86\);

-- Location: FF_X29_Y33_N31
\VGA1|line_counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~85_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(28));

-- Location: MLABCELL_X28_Y32_N27
\VGA1|Add2~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~81_sumout\ = SUM(( \VGA1|line_counter\(29) ) + ( GND ) + ( \VGA1|Add2~86\ ))
-- \VGA1|Add2~82\ = CARRY(( \VGA1|line_counter\(29) ) + ( GND ) + ( \VGA1|Add2~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(29),
	cin => \VGA1|Add2~86\,
	sumout => \VGA1|Add2~81_sumout\,
	cout => \VGA1|Add2~82\);

-- Location: FF_X28_Y32_N47
\VGA1|line_counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~81_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(29));

-- Location: MLABCELL_X28_Y32_N30
\VGA1|Add2~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~77_sumout\ = SUM(( \VGA1|line_counter\(30) ) + ( GND ) + ( \VGA1|Add2~82\ ))
-- \VGA1|Add2~78\ = CARRY(( \VGA1|line_counter\(30) ) + ( GND ) + ( \VGA1|Add2~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_line_counter\(30),
	cin => \VGA1|Add2~82\,
	sumout => \VGA1|Add2~77_sumout\,
	cout => \VGA1|Add2~78\);

-- Location: FF_X28_Y32_N41
\VGA1|line_counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~77_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(30));

-- Location: MLABCELL_X28_Y32_N33
\VGA1|Add2~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add2~113_sumout\ = SUM(( \VGA1|line_counter\(31) ) + ( GND ) + ( \VGA1|Add2~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_line_counter\(31),
	cin => \VGA1|Add2~78\,
	sumout => \VGA1|Add2~113_sumout\);

-- Location: FF_X27_Y33_N50
\VGA1|line_counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add2~113_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|PX_ADDR[20]~7_combout\,
	sload => VCC,
	ena => \VGA1|VGA_VS~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|line_counter\(31));

-- Location: LABCELL_X27_Y33_N15
\VGA1|PX_ADDR[20]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~5_combout\ = ( \VGA1|LessThan1~1_combout\ & ( !\VGA1|line_counter\(31) ) ) # ( !\VGA1|LessThan1~1_combout\ & ( (!\VGA1|line_counter\(31) & !\VGA1|PX_ADDR[20]~4_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101010101010101010100000101000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(31),
	datac => \VGA1|ALT_INV_PX_ADDR[20]~4_combout\,
	datae => \VGA1|ALT_INV_LessThan1~1_combout\,
	combout => \VGA1|PX_ADDR[20]~5_combout\);

-- Location: LABCELL_X29_Y31_N54
\VGA1|LessThan4~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan4~8_combout\ = (\VGA1|LessThan4~4_combout\ & ((!\VGA1|px_counter\(10)) # (\VGA1|LessThan4~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000011001100110000001100110011000000110011001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_LessThan4~4_combout\,
	datac => \VGA1|ALT_INV_LessThan4~7_combout\,
	datad => \VGA1|ALT_INV_px_counter\(10),
	combout => \VGA1|LessThan4~8_combout\);

-- Location: LABCELL_X27_Y35_N27
\VGA1|PX_ADDR[20]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~8_combout\ = ( !\VGA1|LessThan0~2_combout\ & ( \VGA1|LessThan4~8_combout\ & ( !\VGA1|PX_ADDR[20]~5_combout\ ) ) ) # ( \VGA1|LessThan0~2_combout\ & ( !\VGA1|LessThan4~8_combout\ & ( \VGA1|PX_ADDR[20]~7_combout\ ) ) ) # ( 
-- !\VGA1|LessThan0~2_combout\ & ( !\VGA1|LessThan4~8_combout\ & ( !\VGA1|PX_ADDR[20]~5_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010000000001111111110101010101010100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR[20]~5_combout\,
	datad => \VGA1|ALT_INV_PX_ADDR[20]~7_combout\,
	datae => \VGA1|ALT_INV_LessThan0~2_combout\,
	dataf => \VGA1|ALT_INV_LessThan4~8_combout\,
	combout => \VGA1|PX_ADDR[20]~8_combout\);

-- Location: FF_X27_Y35_N32
\VGA1|PX_ADDR[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|Add0~21_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(0));

-- Location: LABCELL_X27_Y35_N33
\VGA1|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~29_sumout\ = SUM(( \VGA1|PX_ADDR\(1) ) + ( GND ) + ( \VGA1|Add0~22\ ))
-- \VGA1|Add0~30\ = CARRY(( \VGA1|PX_ADDR\(1) ) + ( GND ) + ( \VGA1|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(1),
	cin => \VGA1|Add0~22\,
	sumout => \VGA1|Add0~29_sumout\,
	cout => \VGA1|Add0~30\);

-- Location: LABCELL_X27_Y35_N15
\VGA1|PX_ADDR[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[1]~feeder_combout\ = ( \VGA1|Add0~29_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add0~29_sumout\,
	combout => \VGA1|PX_ADDR[1]~feeder_combout\);

-- Location: FF_X27_Y35_N17
\VGA1|PX_ADDR[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[1]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(1));

-- Location: LABCELL_X27_Y35_N36
\VGA1|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~33_sumout\ = SUM(( \VGA1|PX_ADDR[2]~DUPLICATE_q\ ) + ( GND ) + ( \VGA1|Add0~30\ ))
-- \VGA1|Add0~34\ = CARRY(( \VGA1|PX_ADDR[2]~DUPLICATE_q\ ) + ( GND ) + ( \VGA1|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR[2]~DUPLICATE_q\,
	cin => \VGA1|Add0~30\,
	sumout => \VGA1|Add0~33_sumout\,
	cout => \VGA1|Add0~34\);

-- Location: FF_X27_Y35_N20
\VGA1|PX_ADDR[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~33_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR[2]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y35_N39
\VGA1|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~37_sumout\ = SUM(( \VGA1|PX_ADDR\(3) ) + ( GND ) + ( \VGA1|Add0~34\ ))
-- \VGA1|Add0~38\ = CARRY(( \VGA1|PX_ADDR\(3) ) + ( GND ) + ( \VGA1|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(3),
	cin => \VGA1|Add0~34\,
	sumout => \VGA1|Add0~37_sumout\,
	cout => \VGA1|Add0~38\);

-- Location: LABCELL_X27_Y35_N21
\VGA1|PX_ADDR[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[3]~feeder_combout\ = ( \VGA1|Add0~37_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add0~37_sumout\,
	combout => \VGA1|PX_ADDR[3]~feeder_combout\);

-- Location: FF_X27_Y35_N23
\VGA1|PX_ADDR[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[3]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(3));

-- Location: LABCELL_X27_Y35_N42
\VGA1|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~13_sumout\ = SUM(( \VGA1|PX_ADDR\(4) ) + ( GND ) + ( \VGA1|Add0~38\ ))
-- \VGA1|Add0~14\ = CARRY(( \VGA1|PX_ADDR\(4) ) + ( GND ) + ( \VGA1|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(4),
	cin => \VGA1|Add0~38\,
	sumout => \VGA1|Add0~13_sumout\,
	cout => \VGA1|Add0~14\);

-- Location: FF_X27_Y35_N11
\VGA1|PX_ADDR[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~13_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(4));

-- Location: LABCELL_X27_Y35_N45
\VGA1|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~17_sumout\ = SUM(( \VGA1|PX_ADDR\(5) ) + ( GND ) + ( \VGA1|Add0~14\ ))
-- \VGA1|Add0~18\ = CARRY(( \VGA1|PX_ADDR\(5) ) + ( GND ) + ( \VGA1|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(5),
	cin => \VGA1|Add0~14\,
	sumout => \VGA1|Add0~17_sumout\,
	cout => \VGA1|Add0~18\);

-- Location: FF_X27_Y35_N5
\VGA1|PX_ADDR[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~17_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(5));

-- Location: LABCELL_X27_Y35_N48
\VGA1|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~41_sumout\ = SUM(( \VGA1|PX_ADDR\(6) ) + ( GND ) + ( \VGA1|Add0~18\ ))
-- \VGA1|Add0~42\ = CARRY(( \VGA1|PX_ADDR\(6) ) + ( GND ) + ( \VGA1|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(6),
	cin => \VGA1|Add0~18\,
	sumout => \VGA1|Add0~41_sumout\,
	cout => \VGA1|Add0~42\);

-- Location: FF_X27_Y35_N26
\VGA1|PX_ADDR[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~41_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(6));

-- Location: LABCELL_X27_Y35_N51
\VGA1|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~45_sumout\ = SUM(( \VGA1|PX_ADDR\(7) ) + ( GND ) + ( \VGA1|Add0~42\ ))
-- \VGA1|Add0~46\ = CARRY(( \VGA1|PX_ADDR\(7) ) + ( GND ) + ( \VGA1|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(7),
	cin => \VGA1|Add0~42\,
	sumout => \VGA1|Add0~45_sumout\,
	cout => \VGA1|Add0~46\);

-- Location: FF_X27_Y35_N7
\VGA1|PX_ADDR[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~45_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(7));

-- Location: LABCELL_X27_Y35_N54
\VGA1|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~25_sumout\ = SUM(( \VGA1|PX_ADDR\(8) ) + ( GND ) + ( \VGA1|Add0~46\ ))
-- \VGA1|Add0~26\ = CARRY(( \VGA1|PX_ADDR\(8) ) + ( GND ) + ( \VGA1|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(8),
	cin => \VGA1|Add0~46\,
	sumout => \VGA1|Add0~25_sumout\,
	cout => \VGA1|Add0~26\);

-- Location: FF_X27_Y35_N14
\VGA1|PX_ADDR[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~25_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(8));

-- Location: LABCELL_X27_Y35_N57
\VGA1|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~53_sumout\ = SUM(( \VGA1|PX_ADDR\(9) ) + ( GND ) + ( \VGA1|Add0~26\ ))
-- \VGA1|Add0~54\ = CARRY(( \VGA1|PX_ADDR\(9) ) + ( GND ) + ( \VGA1|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(9),
	cin => \VGA1|Add0~26\,
	sumout => \VGA1|Add0~53_sumout\,
	cout => \VGA1|Add0~54\);

-- Location: FF_X27_Y34_N40
\VGA1|PX_ADDR[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~53_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(9));

-- Location: LABCELL_X27_Y34_N0
\VGA1|Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~57_sumout\ = SUM(( \VGA1|PX_ADDR\(10) ) + ( GND ) + ( \VGA1|Add0~54\ ))
-- \VGA1|Add0~58\ = CARRY(( \VGA1|PX_ADDR\(10) ) + ( GND ) + ( \VGA1|Add0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(10),
	cin => \VGA1|Add0~54\,
	sumout => \VGA1|Add0~57_sumout\,
	cout => \VGA1|Add0~58\);

-- Location: FF_X27_Y34_N44
\VGA1|PX_ADDR[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~57_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(10));

-- Location: LABCELL_X27_Y34_N3
\VGA1|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~49_sumout\ = SUM(( \VGA1|PX_ADDR\(11) ) + ( GND ) + ( \VGA1|Add0~58\ ))
-- \VGA1|Add0~50\ = CARRY(( \VGA1|PX_ADDR\(11) ) + ( GND ) + ( \VGA1|Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(11),
	cin => \VGA1|Add0~58\,
	sumout => \VGA1|Add0~49_sumout\,
	cout => \VGA1|Add0~50\);

-- Location: FF_X27_Y34_N38
\VGA1|PX_ADDR[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~49_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(11));

-- Location: LABCELL_X27_Y34_N6
\VGA1|Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~69_sumout\ = SUM(( \VGA1|PX_ADDR\(12) ) + ( GND ) + ( \VGA1|Add0~50\ ))
-- \VGA1|Add0~70\ = CARRY(( \VGA1|PX_ADDR\(12) ) + ( GND ) + ( \VGA1|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(12),
	cin => \VGA1|Add0~50\,
	sumout => \VGA1|Add0~69_sumout\,
	cout => \VGA1|Add0~70\);

-- Location: FF_X27_Y34_N52
\VGA1|PX_ADDR[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~69_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(12));

-- Location: LABCELL_X27_Y34_N9
\VGA1|Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~65_sumout\ = SUM(( \VGA1|PX_ADDR\(13) ) + ( GND ) + ( \VGA1|Add0~70\ ))
-- \VGA1|Add0~66\ = CARRY(( \VGA1|PX_ADDR\(13) ) + ( GND ) + ( \VGA1|Add0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(13),
	cin => \VGA1|Add0~70\,
	sumout => \VGA1|Add0~65_sumout\,
	cout => \VGA1|Add0~66\);

-- Location: MLABCELL_X28_Y34_N0
\VGA1|PX_ADDR[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[13]~feeder_combout\ = ( \VGA1|Add0~65_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add0~65_sumout\,
	combout => \VGA1|PX_ADDR[13]~feeder_combout\);

-- Location: FF_X28_Y34_N2
\VGA1|PX_ADDR[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[13]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(13));

-- Location: LABCELL_X27_Y34_N12
\VGA1|Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~61_sumout\ = SUM(( \VGA1|PX_ADDR\(14) ) + ( GND ) + ( \VGA1|Add0~66\ ))
-- \VGA1|Add0~62\ = CARRY(( \VGA1|PX_ADDR\(14) ) + ( GND ) + ( \VGA1|Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(14),
	cin => \VGA1|Add0~66\,
	sumout => \VGA1|Add0~61_sumout\,
	cout => \VGA1|Add0~62\);

-- Location: FF_X28_Y34_N4
\VGA1|PX_ADDR[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~61_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(14));

-- Location: LABCELL_X27_Y34_N51
\LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = ( \VGA1|PX_ADDR\(14) & ( (\VGA1|PX_ADDR\(13) & \VGA1|PX_ADDR\(12)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(13),
	datad => \VGA1|ALT_INV_PX_ADDR\(12),
	dataf => \VGA1|ALT_INV_PX_ADDR\(14),
	combout => \LessThan0~3_combout\);

-- Location: LABCELL_X27_Y34_N15
\VGA1|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~9_sumout\ = SUM(( \VGA1|PX_ADDR\(15) ) + ( GND ) + ( \VGA1|Add0~62\ ))
-- \VGA1|Add0~10\ = CARRY(( \VGA1|PX_ADDR\(15) ) + ( GND ) + ( \VGA1|Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(15),
	cin => \VGA1|Add0~62\,
	sumout => \VGA1|Add0~9_sumout\,
	cout => \VGA1|Add0~10\);

-- Location: LABCELL_X27_Y34_N36
\VGA1|PX_ADDR[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[15]~feeder_combout\ = \VGA1|Add0~9_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_Add0~9_sumout\,
	combout => \VGA1|PX_ADDR[15]~feeder_combout\);

-- Location: FF_X27_Y34_N37
\VGA1|PX_ADDR[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[15]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(15));

-- Location: LABCELL_X27_Y34_N39
\LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = (!\VGA1|PX_ADDR\(11) & (!\VGA1|PX_ADDR\(10) & !\VGA1|PX_ADDR\(9)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(11),
	datac => \VGA1|ALT_INV_PX_ADDR\(10),
	datad => \VGA1|ALT_INV_PX_ADDR\(9),
	combout => \LessThan0~1_combout\);

-- Location: FF_X27_Y35_N19
\VGA1|PX_ADDR[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~33_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(2));

-- Location: LABCELL_X27_Y35_N6
\LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ( !\VGA1|PX_ADDR\(7) & ( (!\VGA1|PX_ADDR\(6) & (!\VGA1|PX_ADDR\(1) & (!\VGA1|PX_ADDR\(3) & !\VGA1|PX_ADDR\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(6),
	datab => \VGA1|ALT_INV_PX_ADDR\(1),
	datac => \VGA1|ALT_INV_PX_ADDR\(3),
	datad => \VGA1|ALT_INV_PX_ADDR\(2),
	datae => \VGA1|ALT_INV_PX_ADDR\(7),
	combout => \LessThan0~0_combout\);

-- Location: LABCELL_X27_Y35_N3
\LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = ( !\VGA1|PX_ADDR\(5) & ( \VGA1|PX_ADDR\(8) & ( (\LessThan0~1_combout\ & (!\VGA1|PX_ADDR\(0) & (\LessThan0~0_combout\ & !\VGA1|PX_ADDR\(4)))) ) ) ) # ( \VGA1|PX_ADDR\(5) & ( !\VGA1|PX_ADDR\(8) & ( \LessThan0~1_combout\ ) ) ) # ( 
-- !\VGA1|PX_ADDR\(5) & ( !\VGA1|PX_ADDR\(8) & ( \LessThan0~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000100000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~1_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR\(0),
	datac => \ALT_INV_LessThan0~0_combout\,
	datad => \VGA1|ALT_INV_PX_ADDR\(4),
	datae => \VGA1|ALT_INV_PX_ADDR\(5),
	dataf => \VGA1|ALT_INV_PX_ADDR\(8),
	combout => \LessThan0~2_combout\);

-- Location: LABCELL_X27_Y34_N18
\VGA1|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~5_sumout\ = SUM(( \VGA1|PX_ADDR\(16) ) + ( GND ) + ( \VGA1|Add0~10\ ))
-- \VGA1|Add0~6\ = CARRY(( \VGA1|PX_ADDR\(16) ) + ( GND ) + ( \VGA1|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(16),
	cin => \VGA1|Add0~10\,
	sumout => \VGA1|Add0~5_sumout\,
	cout => \VGA1|Add0~6\);

-- Location: FF_X27_Y35_N1
\VGA1|PX_ADDR[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~5_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(16));

-- Location: LABCELL_X27_Y34_N21
\VGA1|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~1_sumout\ = SUM(( \VGA1|PX_ADDR\(17) ) + ( GND ) + ( \VGA1|Add0~6\ ))
-- \VGA1|Add0~2\ = CARRY(( \VGA1|PX_ADDR\(17) ) + ( GND ) + ( \VGA1|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(17),
	cin => \VGA1|Add0~6\,
	sumout => \VGA1|Add0~1_sumout\,
	cout => \VGA1|Add0~2\);

-- Location: FF_X27_Y34_N34
\VGA1|PX_ADDR[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~1_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(17));

-- Location: LABCELL_X27_Y34_N24
\VGA1|Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~81_sumout\ = SUM(( \VGA1|PX_ADDR\(18) ) + ( GND ) + ( \VGA1|Add0~2\ ))
-- \VGA1|Add0~82\ = CARRY(( \VGA1|PX_ADDR\(18) ) + ( GND ) + ( \VGA1|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA1|ALT_INV_PX_ADDR\(18),
	cin => \VGA1|Add0~2\,
	sumout => \VGA1|Add0~81_sumout\,
	cout => \VGA1|Add0~82\);

-- Location: MLABCELL_X28_Y34_N6
\VGA1|PX_ADDR[18]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[18]~feeder_combout\ = ( \VGA1|Add0~81_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add0~81_sumout\,
	combout => \VGA1|PX_ADDR[18]~feeder_combout\);

-- Location: FF_X28_Y34_N8
\VGA1|PX_ADDR[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[18]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(18));

-- Location: LABCELL_X27_Y34_N27
\VGA1|Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~77_sumout\ = SUM(( \VGA1|PX_ADDR\(19) ) + ( GND ) + ( \VGA1|Add0~82\ ))
-- \VGA1|Add0~78\ = CARRY(( \VGA1|PX_ADDR\(19) ) + ( GND ) + ( \VGA1|Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_PX_ADDR\(19),
	cin => \VGA1|Add0~82\,
	sumout => \VGA1|Add0~77_sumout\,
	cout => \VGA1|Add0~78\);

-- Location: FF_X27_Y34_N50
\VGA1|PX_ADDR[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \VGA1|Add0~77_sumout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	sload => VCC,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(19));

-- Location: LABCELL_X27_Y34_N30
\VGA1|Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|Add0~73_sumout\ = SUM(( \VGA1|PX_ADDR\(20) ) + ( GND ) + ( \VGA1|Add0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(20),
	cin => \VGA1|Add0~78\,
	sumout => \VGA1|Add0~73_sumout\);

-- Location: LABCELL_X27_Y34_N45
\VGA1|PX_ADDR[20]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|PX_ADDR[20]~feeder_combout\ = ( \VGA1|Add0~73_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \VGA1|ALT_INV_Add0~73_sumout\,
	combout => \VGA1|PX_ADDR[20]~feeder_combout\);

-- Location: FF_X27_Y34_N47
\VGA1|PX_ADDR[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|PX_ADDR[20]~feeder_combout\,
	clrn => \SW[0]~input_o\,
	sclr => \VGA1|LessThan0~2_combout\,
	ena => \VGA1|PX_ADDR[20]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|PX_ADDR\(20));

-- Location: LABCELL_X27_Y34_N48
\LessThan0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~4_combout\ = (!\VGA1|PX_ADDR\(20) & (!\VGA1|PX_ADDR\(18) & !\VGA1|PX_ADDR\(19)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_PX_ADDR\(20),
	datac => \VGA1|ALT_INV_PX_ADDR\(18),
	datad => \VGA1|ALT_INV_PX_ADDR\(19),
	combout => \LessThan0~4_combout\);

-- Location: LABCELL_X27_Y34_N54
\LessThan0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~5_combout\ = ( \LessThan0~4_combout\ & ( \VGA1|PX_ADDR\(16) & ( \VGA1|PX_ADDR\(17) ) ) ) # ( !\LessThan0~4_combout\ & ( \VGA1|PX_ADDR\(16) ) ) # ( \LessThan0~4_combout\ & ( !\VGA1|PX_ADDR\(16) & ( (\VGA1|PX_ADDR\(17) & (((\LessThan0~3_combout\ 
-- & !\LessThan0~2_combout\)) # (\VGA1|PX_ADDR\(15)))) ) ) ) # ( !\LessThan0~4_combout\ & ( !\VGA1|PX_ADDR\(16) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000111001111111111111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~3_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR\(15),
	datac => \ALT_INV_LessThan0~2_combout\,
	datad => \VGA1|ALT_INV_PX_ADDR\(17),
	datae => \ALT_INV_LessThan0~4_combout\,
	dataf => \VGA1|ALT_INV_PX_ADDR\(16),
	combout => \LessThan0~5_combout\);

-- Location: LABCELL_X22_Y79_N0
\VGA1|VGA_R[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[0]~1_combout\ = ( !\LessThan0~5_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[0]~1_combout\);

-- Location: LABCELL_X27_Y33_N6
\VGA1|VGA_R[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[0]~0_combout\ = ( \VGA1|LessThan1~1_combout\ & ( \VGA1|px_counter\(31) & ( (\VGA1|line_counter\(31) & \SW[0]~input_o\) ) ) ) # ( !\VGA1|LessThan1~1_combout\ & ( \VGA1|px_counter\(31) & ( (\SW[0]~input_o\ & ((\VGA1|PX_ADDR[20]~4_combout\) # 
-- (\VGA1|line_counter\(31)))) ) ) ) # ( \VGA1|LessThan1~1_combout\ & ( !\VGA1|px_counter\(31) & ( (\VGA1|line_counter\(31) & (\SW[0]~input_o\ & \VGA1|LessThan0~1_combout\)) ) ) ) # ( !\VGA1|LessThan1~1_combout\ & ( !\VGA1|px_counter\(31) & ( 
-- (\SW[0]~input_o\ & (\VGA1|LessThan0~1_combout\ & ((\VGA1|PX_ADDR[20]~4_combout\) # (\VGA1|line_counter\(31))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000111000000000000010100000111000001110000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(31),
	datab => \VGA1|ALT_INV_PX_ADDR[20]~4_combout\,
	datac => \ALT_INV_SW[0]~input_o\,
	datad => \VGA1|ALT_INV_LessThan0~1_combout\,
	datae => \VGA1|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA1|ALT_INV_px_counter\(31),
	combout => \VGA1|VGA_R[0]~0_combout\);

-- Location: FF_X22_Y79_N1
\VGA1|VGA_R[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[0]~1_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(0));

-- Location: LABCELL_X22_Y79_N39
\VGA1|VGA_R[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[1]~2_combout\ = ( !\LessThan0~5_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[1]~2_combout\);

-- Location: FF_X22_Y79_N40
\VGA1|VGA_R[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[1]~2_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(1));

-- Location: LABCELL_X22_Y79_N42
\VGA1|VGA_R[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[2]~3_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[2]~3_combout\);

-- Location: FF_X22_Y79_N44
\VGA1|VGA_R[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[2]~3_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(2));

-- Location: LABCELL_X22_Y79_N45
\VGA1|VGA_R[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[3]~4_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[3]~4_combout\);

-- Location: FF_X22_Y79_N46
\VGA1|VGA_R[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[3]~4_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(3));

-- Location: LABCELL_X22_Y79_N48
\VGA1|VGA_R[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[4]~5_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[4]~5_combout\);

-- Location: FF_X22_Y79_N49
\VGA1|VGA_R[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[4]~5_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(4));

-- Location: LABCELL_X22_Y79_N51
\VGA1|VGA_R[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[5]~6_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[5]~6_combout\);

-- Location: FF_X22_Y79_N52
\VGA1|VGA_R[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[5]~6_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(5));

-- Location: LABCELL_X22_Y79_N54
\VGA1|VGA_R[6]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[6]~7_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[6]~7_combout\);

-- Location: FF_X22_Y79_N56
\VGA1|VGA_R[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[6]~7_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(6));

-- Location: LABCELL_X22_Y79_N57
\VGA1|VGA_R[7]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_R[7]~8_combout\ = !\LessThan0~5_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_R[7]~8_combout\);

-- Location: FF_X22_Y79_N58
\VGA1|VGA_R[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_R[7]~8_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_R\(7));

-- Location: FF_X27_Y34_N59
\VGA1|VGA_G[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \LessThan0~5_combout\,
	sload => VCC,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(0));

-- Location: FF_X22_Y79_N4
\VGA1|VGA_G[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \LessThan0~5_combout\,
	sload => VCC,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(1));

-- Location: FF_X22_Y79_N32
\VGA1|VGA_G[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \LessThan0~5_combout\,
	sload => VCC,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(2));

-- Location: LABCELL_X22_Y79_N33
\VGA1|VGA_G[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_G[3]~feeder_combout\ = ( \LessThan0~5_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_G[3]~feeder_combout\);

-- Location: FF_X22_Y79_N34
\VGA1|VGA_G[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_G[3]~feeder_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(3));

-- Location: FF_X22_Y79_N7
\VGA1|VGA_G[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \LessThan0~5_combout\,
	sload => VCC,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(4));

-- Location: LABCELL_X22_Y79_N9
\VGA1|VGA_G[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_G[5]~feeder_combout\ = ( \LessThan0~5_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \VGA1|VGA_G[5]~feeder_combout\);

-- Location: FF_X22_Y79_N10
\VGA1|VGA_G[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_G[5]~feeder_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(5));

-- Location: FF_X22_Y79_N37
\VGA1|VGA_G[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	asdata => \LessThan0~5_combout\,
	sload => VCC,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(6));

-- Location: FF_X27_Y34_N56
\VGA1|VGA_G[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \LessThan0~5_combout\,
	ena => \VGA1|VGA_R[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_G\(7));

-- Location: LABCELL_X29_Y31_N18
\VGA1|LessThan3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan3~0_combout\ = ( !\VGA1|px_counter\(10) & ( (\VGA1|LessThan4~1_combout\ & (\VGA1|LessThan4~0_combout\ & (\VGA1|LessThan4~2_combout\ & \VGA1|LessThan4~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan4~1_combout\,
	datab => \VGA1|ALT_INV_LessThan4~0_combout\,
	datac => \VGA1|ALT_INV_LessThan4~2_combout\,
	datad => \VGA1|ALT_INV_LessThan4~3_combout\,
	dataf => \VGA1|ALT_INV_px_counter\(10),
	combout => \VGA1|LessThan3~0_combout\);

-- Location: LABCELL_X29_Y31_N45
\VGA1|LessThan3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan3~1_combout\ = ( \VGA1|px_counter\(3) & ( \VGA1|px_counter\(6) ) ) # ( !\VGA1|px_counter\(3) & ( (\VGA1|px_counter\(6) & ((\VGA1|px_counter\(4)) # (\VGA1|px_counter\(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001111000001010000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(5),
	datac => \VGA1|ALT_INV_px_counter\(6),
	datad => \VGA1|ALT_INV_px_counter\(4),
	dataf => \VGA1|ALT_INV_px_counter\(3),
	combout => \VGA1|LessThan3~1_combout\);

-- Location: LABCELL_X29_Y31_N39
\VGA1|LessThan2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan2~0_combout\ = ( \VGA1|px_counter\(9) & ( (\VGA1|px_counter\(8) & ((\VGA1|px_counter\(7)) # (\VGA1|LessThan3~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011001100110000001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(8),
	datac => \VGA1|ALT_INV_LessThan3~1_combout\,
	datad => \VGA1|ALT_INV_px_counter\(7),
	dataf => \VGA1|ALT_INV_px_counter\(9),
	combout => \VGA1|LessThan2~0_combout\);

-- Location: LABCELL_X29_Y33_N21
\VGA1|VGA_VS~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_VS~0_combout\ = ( \VGA1|LessThan3~0_combout\ & ( \VGA1|LessThan2~0_combout\ & ( (\VGA1|LessThan0~0_combout\ & !\VGA1|px_counter\(31)) ) ) ) # ( !\VGA1|LessThan3~0_combout\ & ( \VGA1|LessThan2~0_combout\ & ( !\VGA1|px_counter\(31) ) ) ) # ( 
-- !\VGA1|LessThan3~0_combout\ & ( !\VGA1|LessThan2~0_combout\ & ( !\VGA1|px_counter\(31) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011110000111100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan0~0_combout\,
	datac => \VGA1|ALT_INV_px_counter\(31),
	datae => \VGA1|ALT_INV_LessThan3~0_combout\,
	dataf => \VGA1|ALT_INV_LessThan2~0_combout\,
	combout => \VGA1|VGA_VS~0_combout\);

-- Location: LABCELL_X29_Y31_N48
\VGA1|VGA_BLANK_N~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_BLANK_N~0_combout\ = ( \VGA1|VGA_VS~0_combout\ & ( (!\VGA1|px_counter\(31) & (((!\VGA1|LessThan0~2_combout\ & !\VGA1|PX_ADDR[20]~5_combout\)) # (\VGA1|VGA_BLANK_N~q\))) # (\VGA1|px_counter\(31) & (!\VGA1|LessThan0~2_combout\ & 
-- (!\VGA1|PX_ADDR[20]~5_combout\))) ) ) # ( !\VGA1|VGA_VS~0_combout\ & ( (!\VGA1|LessThan0~2_combout\ & !\VGA1|PX_ADDR[20]~5_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000011000000111010101100000011101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_px_counter\(31),
	datab => \VGA1|ALT_INV_LessThan0~2_combout\,
	datac => \VGA1|ALT_INV_PX_ADDR[20]~5_combout\,
	datad => \VGA1|ALT_INV_VGA_BLANK_N~q\,
	dataf => \VGA1|ALT_INV_VGA_VS~0_combout\,
	combout => \VGA1|VGA_BLANK_N~0_combout\);

-- Location: FF_X29_Y31_N50
\VGA1|VGA_BLANK_N\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_BLANK_N~0_combout\,
	ena => \SW[0]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_BLANK_N~q\);

-- Location: LABCELL_X29_Y31_N42
\VGA1|LessThan3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan3~2_combout\ = ( \VGA1|px_counter\(8) & ( (\VGA1|px_counter\(7) & (\VGA1|px_counter\(9) & \VGA1|LessThan3~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_px_counter\(7),
	datac => \VGA1|ALT_INV_px_counter\(9),
	datad => \VGA1|ALT_INV_LessThan3~1_combout\,
	dataf => \VGA1|ALT_INV_px_counter\(8),
	combout => \VGA1|LessThan3~2_combout\);

-- Location: LABCELL_X29_Y33_N6
\VGA1|VGA_HS~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_HS~0_combout\ = ( \VGA1|VGA_HS~q\ & ( \VGA1|LessThan4~8_combout\ & ( (!\VGA1|LessThan2~0_combout\) # ((!\VGA1|LessThan3~0_combout\) # ((!\VGA1|LessThan0~2_combout\) # (\VGA1|LessThan3~2_combout\))) ) ) ) # ( !\VGA1|VGA_HS~q\ & ( 
-- \VGA1|LessThan4~8_combout\ & ( (!\VGA1|LessThan3~0_combout\) # ((!\VGA1|LessThan0~2_combout\) # ((\VGA1|LessThan2~0_combout\ & \VGA1|LessThan3~2_combout\))) ) ) ) # ( \VGA1|VGA_HS~q\ & ( !\VGA1|LessThan4~8_combout\ & ( (!\VGA1|LessThan2~0_combout\) # 
-- ((!\VGA1|LessThan3~0_combout\) # ((!\VGA1|LessThan0~2_combout\) # (\VGA1|LessThan3~2_combout\))) ) ) ) # ( !\VGA1|VGA_HS~q\ & ( !\VGA1|LessThan4~8_combout\ & ( !\VGA1|LessThan0~2_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111111110111111111111110011011111111111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan2~0_combout\,
	datab => \VGA1|ALT_INV_LessThan3~0_combout\,
	datac => \VGA1|ALT_INV_LessThan3~2_combout\,
	datad => \VGA1|ALT_INV_LessThan0~2_combout\,
	datae => \VGA1|ALT_INV_VGA_HS~q\,
	dataf => \VGA1|ALT_INV_LessThan4~8_combout\,
	combout => \VGA1|VGA_HS~0_combout\);

-- Location: FF_X29_Y33_N8
\VGA1|VGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_HS~0_combout\,
	ena => \SW[0]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_HS~q\);

-- Location: LABCELL_X27_Y33_N57
\VGA1|LessThan6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan6~1_combout\ = ( \VGA1|LessThan1~0_combout\ & ( (\VGA1|line_counter\(9) & (((!\VGA1|LessThan6~0_combout\) # (\VGA1|line_counter\(8))) # (\VGA1|line_counter\(7)))) ) ) # ( !\VGA1|LessThan1~0_combout\ & ( (\VGA1|line_counter\(9) & 
-- ((\VGA1|line_counter\(8)) # (\VGA1|line_counter\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100110011000100010011001100110001001100110011000100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(7),
	datab => \VGA1|ALT_INV_line_counter\(9),
	datac => \VGA1|ALT_INV_LessThan6~0_combout\,
	datad => \VGA1|ALT_INV_line_counter\(8),
	dataf => \VGA1|ALT_INV_LessThan1~0_combout\,
	combout => \VGA1|LessThan6~1_combout\);

-- Location: LABCELL_X27_Y33_N51
\VGA1|VGA_VS~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_VS~2_combout\ = (\VGA1|LessThan5~1_combout\ & (\VGA1|PX_ADDR[20]~4_combout\ & !\VGA1|LessThan6~1_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010000000100000001000000010000000100000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan5~1_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR[20]~4_combout\,
	datac => \VGA1|ALT_INV_LessThan6~1_combout\,
	combout => \VGA1|VGA_VS~2_combout\);

-- Location: LABCELL_X27_Y33_N54
\VGA1|LessThan7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|LessThan7~2_combout\ = ( \VGA1|LessThan7~1_combout\ & ( (\VGA1|line_counter\(9) & (((!\VGA1|LessThan7~0_combout\) # (\VGA1|line_counter\(8))) # (\VGA1|line_counter\(7)))) ) ) # ( !\VGA1|LessThan7~1_combout\ & ( (\VGA1|line_counter\(9) & 
-- ((\VGA1|line_counter\(8)) # (\VGA1|line_counter\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100110011000100010011001100110001001100110011000100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_line_counter\(7),
	datab => \VGA1|ALT_INV_line_counter\(9),
	datac => \VGA1|ALT_INV_LessThan7~0_combout\,
	datad => \VGA1|ALT_INV_line_counter\(8),
	dataf => \VGA1|ALT_INV_LessThan7~1_combout\,
	combout => \VGA1|LessThan7~2_combout\);

-- Location: LABCELL_X27_Y33_N36
\VGA1|VGA_VS~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_VS~3_combout\ = ( \VGA1|LessThan1~1_combout\ & ( \VGA1|LessThan5~1_combout\ & ( (!\VGA1|line_counter\(31) & (((!\VGA1|PX_ADDR[20]~4_combout\) # (!\VGA1|LessThan6~1_combout\)) # (\VGA1|LessThan7~2_combout\))) ) ) ) # ( !\VGA1|LessThan1~1_combout\ 
-- & ( \VGA1|LessThan5~1_combout\ & ( (!\VGA1|PX_ADDR[20]~4_combout\ & !\VGA1|line_counter\(31)) ) ) ) # ( \VGA1|LessThan1~1_combout\ & ( !\VGA1|LessThan5~1_combout\ & ( !\VGA1|line_counter\(31) ) ) ) # ( !\VGA1|LessThan1~1_combout\ & ( 
-- !\VGA1|LessThan5~1_combout\ & ( (!\VGA1|PX_ADDR[20]~4_combout\ & !\VGA1|line_counter\(31)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000111100001111000011000000110000001111000011010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_LessThan7~2_combout\,
	datab => \VGA1|ALT_INV_PX_ADDR[20]~4_combout\,
	datac => \VGA1|ALT_INV_line_counter\(31),
	datad => \VGA1|ALT_INV_LessThan6~1_combout\,
	datae => \VGA1|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA1|ALT_INV_LessThan5~1_combout\,
	combout => \VGA1|VGA_VS~3_combout\);

-- Location: LABCELL_X29_Y33_N12
\VGA1|VGA_VS~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_VS~4_combout\ = ( \VGA1|VGA_VS~q\ & ( \VGA1|VGA_VS~3_combout\ & ( (!\VGA1|VGA_VS~1_combout\) # ((!\VGA1|VGA_VS~2_combout\) # ((!\VGA1|VGA_VS~0_combout\) # (!\SW[0]~input_o\))) ) ) ) # ( \VGA1|VGA_VS~q\ & ( !\VGA1|VGA_VS~3_combout\ ) ) # ( 
-- !\VGA1|VGA_VS~q\ & ( !\VGA1|VGA_VS~3_combout\ & ( (\VGA1|VGA_VS~1_combout\ & (\VGA1|VGA_VS~0_combout\ & \SW[0]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101111111111111111100000000000000001111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA1|ALT_INV_VGA_VS~1_combout\,
	datab => \VGA1|ALT_INV_VGA_VS~2_combout\,
	datac => \VGA1|ALT_INV_VGA_VS~0_combout\,
	datad => \ALT_INV_SW[0]~input_o\,
	datae => \VGA1|ALT_INV_VGA_VS~q\,
	dataf => \VGA1|ALT_INV_VGA_VS~3_combout\,
	combout => \VGA1|VGA_VS~4_combout\);

-- Location: FF_X29_Y33_N14
\VGA1|VGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL_VGA|pll_1028_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	d => \VGA1|VGA_VS~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA1|VGA_VS~q\);

-- Location: LABCELL_X29_Y33_N36
\VGA1|VGA_SYNC_N~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA1|VGA_SYNC_N~0_combout\ = ( \VGA1|VGA_HS~q\ & ( !\VGA1|VGA_VS~q\ ) ) # ( !\VGA1|VGA_HS~q\ & ( \VGA1|VGA_VS~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA1|ALT_INV_VGA_VS~q\,
	dataf => \VGA1|ALT_INV_VGA_HS~q\,
	combout => \VGA1|VGA_SYNC_N~0_combout\);

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X2_Y0_N41
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X16_Y0_N18
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: IOIBUF_X4_Y0_N35
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: IOIBUF_X4_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X4_Y0_N18
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X2_Y0_N58
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LABCELL_X23_Y65_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;


