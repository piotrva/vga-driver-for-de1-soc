library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity vga_simple is
	port(
		VGA_R			: out std_logic_vector(7 downto 0);
		VGA_G			: out std_logic_vector(7 downto 0);
		VGA_B			: out std_logic_vector(7 downto 0);
		VGA_CLK		: out std_logic;
		VGA_BLANK_N	: out std_logic;
		VGA_HS		: buffer std_logic;
		VGA_VS		: buffer std_logic;
		VGA_SYNC_N	: out std_logic;
		CLOCK_50		: in std_logic;
		SW				: in std_logic_vector(9 downto 0)
	);
end vga_simple;

architecture basic of vga_simple is

	component PLL_1028 is
		port (
			refclk   : in  std_logic := '0'; --  refclk.clk
			rst      : in  std_logic := '0'; --   reset.reset
			outclk_0 : out std_logic;         -- outclk0.clk
			outclk_1 : out std_logic         -- outclk1.clk
		);
	end component PLL_1028;
	
	component vga_driver is
		generic(
			px_width				: integer := 800;
			px_front_porch		: integer := 40;
			px_sync_pulse		: integer := 128;
			px_back_porch		: integer := 88;
			line_height			: integer := 600;
			line_front_porch	: integer := 1;
			line_sync_pulse	: integer := 4;
			line_back_porch	: integer := 23
		);
		port(
			VGA_R			: out std_logic_vector(7 downto 0);
			VGA_G			: out std_logic_vector(7 downto 0);
			VGA_B			: out std_logic_vector(7 downto 0);
			VGA_IN		: in std_logic_vector(23 downto 0);
			VGA_CLK		: out std_logic;
			VGA_BLANK_N	: out std_logic;
			VGA_HS		: buffer std_logic;
			VGA_VS		: buffer std_logic;
			VGA_SYNC_N	: out std_logic;
			PX_ADDR		: buffer std_logic_vector(20 downto 0);
			CLK_PIX		: in std_logic;
			RST			: in std_logic
		);
	end component vga_driver;
	
	signal clk_40MHz : std_logic := '0';
	signal clk_108MHz : std_logic;
	signal raddress : std_logic_vector(18 downto 0);
	signal pix_data : std_logic_vector(23 downto 0);
	signal PX_ADDR : std_logic_vector(20 downto 0);
begin

	PLL_VGA : PLL_1028 port map(refclk => CLOCK_50, rst => '0', 
		outclk_0 => clk_40MHz, outclk_1 => clk_108MHz);
	
	VGA1 : vga_driver
	--generic map(
	--	px_width				=> 1280,
	--	px_front_porch		=> 48,
	--	px_sync_pulse		=> 112,
	--	px_back_porch		=> 248,
	--	line_height			=> 1024,
	--	line_front_porch	=> 1,
	--	line_sync_pulse	=> 3,
	--	line_back_porch	=> 38
	--	
	--) 
	port map(
		VGA_R => VGA_R,
		VGA_G => VGA_G,
		VGA_B => VGA_B,
		VGA_IN => pix_data,
		VGA_CLK => VGA_CLK,
		VGA_BLANK_N => VGA_BLANK_N,
		VGA_HS => VGA_HS,
		VGA_VS => VGA_VS,
		VGA_SYNC_N => VGA_SYNC_N,
		CLK_PIX => clk_40MHz,
		PX_ADDR => PX_ADDR,
		RST => SW(0)
	);
	
	raddress <= PX_ADDR(18 downto 0);
	
	pix_data <= "000000001111111100000000" when (PX_ADDR>160000) else "111111110000000000000000";
	
	
	
end basic;
